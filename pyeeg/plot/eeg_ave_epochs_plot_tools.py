import numpy as np
import matplotlib.pyplot as plt
from pyeeg.processing.tools.peak_detection.time_domain_tools import get_channel_peaks_and_windows
from pyeeg.definitions.eeg_definitions import EegChannel, Domain
import os
from pyeeg.plot.eeg_plot_tools import interpolate_potential_fields, get_3D_spherical_positions
import gc
import matplotlib.gridspec as gridspec
from scipy import signal
from operator import itemgetter
import pycwt as wavelet
import pywt as pw
from pyeeg.processing.pipe.definitions import DataNode
import astropy.units as u


def get_potential_fields(epochs_ave: DataNode = DataNode(),
                         x_val: np.array = np.array([]),
                         domain: Domain = Domain.time,
                         **kwargs):
    grid = kwargs.get('grid', 1000j)
    if domain == Domain.time:
        potentials = np.squeeze(epochs_ave.data[epochs_ave.x_to_samples(x_val), :])
    if domain == Domain.frequency:
        potentials = np.abs(epochs_ave.data)[epochs_ave.x_to_samples(x_val), :]

    x = np.zeros((potentials.size, 1))
    y = np.zeros((potentials.size, 1))
    z = np.zeros((potentials.size, 1))
    for i, _ch in enumerate(epochs_ave.layout):
        if (_ch.x is not None and not np.isnan(_ch.x)) and (_ch.y is not None and not np.isnan(_ch.y)):
            x[i] = _ch.x
            y[i] = _ch.y
            z[i] = potentials[i]
    interpolated_potentials = interpolate_potential_fields(x, y, z, **kwargs)
    return interpolated_potentials


def plot_time_single_channels(ave_data=DataNode(),
                              figure_dir_path: str = None,
                              figure_basename: str = None,
                              fig_format: str = '.png',
                              offset_step=None,
                              idx_ch_to_plot=None,
                              title='',
                              x_lim=None,
                              y_lim=None,
                              fontsize=8):
    """
    :param ave_data: DataNode class containing data to plot
    :param figure_dir_path: directory path where figure will be saved
    :param figure_basename: output file name
    :param fig_format: output format of generated figure (.pdf, .png)
    :param idx_ch_to_plot: index of channels to be plot
    :param offset_step: constant to vertically separate each channel
    :param title: desired figure title
    :param x_lim: plot x limits
    :param y_lim: plot x limits
    :param fontsize: fontsize of text in figures
    :return:
    """
    ch_idx = idx_ch_to_plot if idx_ch_to_plot is not None else list(range(ave_data.data.shape[1]))
    ch_idx = np.minimum(ch_idx, ave_data.data.shape[1] - 1)
    ch_idx = np.unique(ch_idx)
    n_ch_to_plot = np.minimum(ave_data.data.shape[1], ch_idx.size)
    # plot individual channels
    font = {'size': 6}
    _fig_path = figure_dir_path + figure_basename + '_time_' + fig_format
    if not offset_step:
        offset_step = np.max(np.max(np.abs(ave_data.data), axis=0))
    if x_lim is None:
        x_lim = [0, ave_data.x[-1]]
    offset_vector = np.arange(n_ch_to_plot) * offset_step
    ave_w = ave_data.data[:, ch_idx] - offset_vector.reshape(1, -1)
    fig, ax1 = plt.subplots()
    plt.rc('font', **font)
    ax1.plot(ave_data.x, ave_w, color='g', linewidth=0.5)
    # add detected peaks
    _channels = itemgetter(*ch_idx)(ave_data.layout)
    for i, _ch in enumerate(_channels):
        _ch_peaks, _time_windows = get_channel_peaks_and_windows(eeg_peaks=ave_data.peak_times,
                                                                 eeg_time_windows=ave_data.peak_time_windows,
                                                                 channel_label=_ch.label)
        for j, _peak in enumerate(_ch_peaks):
            if _peak.peak_label and _peak.show_label:
                markerfacecolor = 'black' if _peak.significant else 'white'
                if _peak.positive:
                    ax1.plot(_peak.x, _peak.amp - offset_vector[i], '^', markersize=0.5,
                             markerfacecolor=markerfacecolor)
                else:
                    ax1.plot(_peak.x, _peak.amp - offset_vector[i], 'v', markersize=0.5,
                             markerfacecolor=markerfacecolor)
                if _peak.show_label:
                    ax1.text(_peak.x, _peak.amp * 1.1 - offset_vector[i], _peak.peak_label,
                             horizontalalignment='center',
                             verticalalignment='bottom')
        if _time_windows:
            [ax1.fill_between([_t_w.ini_time, _t_w.end_time],
                              [-offset_vector[i] - offset_step / 4.0, -offset_vector[i] - offset_step / 4.0],
                              [-offset_vector[i] + offset_step / 4.0, -offset_vector[i] + offset_step / 4.0],
                              alpha=0.15, facecolor='r')
             for _t_w in ave_data.peak_time_windows
             if _t_w.positive_peak and _t_w.show_window and _t_w.channel == _ch.label and _t_w.ini_time is not None
             and _t_w.end_time is not None]
            [ax1.fill_between([_t_w.ini_time, _t_w.end_time],
                              [-offset_vector[i] - offset_step / 4.0, -offset_vector[i] - offset_step / 4.0],
                              [-offset_vector[i] + offset_step / 4.0, -offset_vector[i] + offset_step / 4.0],
                              alpha=0.15, facecolor='b')
             for _t_w in ave_data.peak_time_windows
             if not _t_w.positive_peak and _t_w.show_window and _t_w.show_window and _t_w.channel == _ch.label and
             _t_w.ini_time is not None and _t_w.end_time is not None]

    ax1.set_title(title, fontsize=fontsize)
    ax1.set_xlim(x_lim)
    if y_lim is None:
        ax1.set_ylim(-(n_ch_to_plot + 1) * offset_step, offset_step)
    else:
        ax1.set_ylim(y_lim)

    ax1.set_xlabel('Time [{:}]'.format(ave_data.x_units),
                   fontsize=fontsize)
    ax1.set_ylabel('Amplitude [{:}]'.format(ave_data.y_units),
                   fontsize=fontsize)
    ax2 = ax1.twinx()
    ax2.set_ylim(ax1.get_ylim())
    ax2.set_yticks(-offset_vector)
    ax2.set_yticklabels([_ch.label for _ch in _channels], fontsize=fontsize)
    ax2.spines["right"].set_position(("axes", - 0.1))
    fig.savefig(_fig_path, bbox_inches='tight')
    # plt.show()
    fig.clf()
    plt.close()
    gc.collect()
    print('time plots saved figure: {:}'.format(_fig_path))


def plot_freq_single_channels(ave_data=DataNode(),
                              file_name='',
                              offset_step=None,
                              title='',
                              idx_ch_to_plot=None,
                              x_lim=None,
                              y_lim=None):
    ch_idx = idx_ch_to_plot if idx_ch_to_plot is not None else list(range(ave_data.data.shape[1]))
    ch_idx = np.minimum(ch_idx, ave_data.data.shape[1])
    n_ch_to_plot = np.minimum(ave_data.data.shape[1], ch_idx.size)

    # plot individual channels
    font = {'size': 6}
    if not offset_step:
        offset_step = np.max(np.max(np.abs(ave_data.rfft_average), axis=0))
    if x_lim is None:
        x_lim = [0, ave_data.x[-1]]
    offset_vector = np.arange(ave_data.data.shape[1]) * offset_step
    fft_w = np.fft.rfft(ave_data.data) - offset_vector.reshape(1, -1)
    fig, ax1 = plt.subplots()
    plt.rc('font', **font)
    ax1.plot(ave_data.x, fft_w, color='g', linewidth=0.5)

    # add detected peaks
    _channels = itemgetter(*ch_idx)(ave_data.channels)
    for i, _ch in enumerate(_channels):
        _ch_peaks, _ = get_channel_peaks_and_windows(eeg_peaks=ave_data.peak_frequency, channel_label=_ch.label)
        for j, _peak in enumerate(_ch_peaks):
            if _peak.peak_label and _peak.show_label:
                markerfacecolor = 'black' if _peak.significant else 'white'
                if _peak.positive:
                    ax1.plot(_peak.x, _peak.amp - offset_vector[i], '^', markersize=0.5,
                             markerfacecolor=markerfacecolor)
                else:
                    ax1.plot(_peak.x, _peak.amp - offset_vector[i], 'v', markersize=0.5,
                             markerfacecolor=markerfacecolor)
                if _peak.show_label:
                    ax1.text(_peak.x, _peak.amp * 1.1 - offset_vector[i], _peak.peak_label,
                             horizontalalignment='center',
                             verticalalignment='bottom')

    ax1.set_title(title)
    ax1.set_xlim(x_lim)
    if y_lim is None:
        ax1.set_ylim(-(n_ch_to_plot + 1) * offset_step, offset_step)
    else:
        ax1.set_ylim(y_lim)

    ax1.set_xlabel('Frequency [{:}]'.format(ave_data.x_units))
    ax1.set_ylabel('Amplitude [{:}]'.format(ave_data.y_units))
    ax2 = ax1.twinx()
    ax2.set_ylim(ax1.get_ylim())
    ax2.set_yticks(-offset_vector)
    ax2.set_yticklabels([_ch.label for _ch in ave_data.channels])
    ax2.spines["right"].set_position(("axes", - 0.1))
    fig.savefig(file_name, bbox_inches='tight')
    # plt.show()
    fig.clf()
    plt.close()
    gc.collect()
    print(('saved figure: %s' % file_name))


def plot_time_peak_topographic_map(ave_data=DataNode(),
                                   file_name='',
                                   title='',
                                   channel_label=[],
                                   x_lim=None,
                                   y_lim=None,
                                   fontsize=8):
    if x_lim is None:
        x_lim = [ave_data.x[0], ave_data.x[-1]]
    grid_size = 500j
    peak_scalp_potentials = []

    _ch_idx = [_i for _i, _ch in enumerate(ave_data.layout) if _ch.x is not None and _ch.y is not None]
    assigned_channels = ave_data.layout[_ch_idx]

    if not channel_label:
        channel_label = assigned_channels[0].label

    dummy_var = np.array([_i for _i, _ch in enumerate(assigned_channels) if _ch.label == channel_label])
    if not len(dummy_var):
        return
    idx_ch = ave_data.get_channel_idx_by_label([channel_label])

    # check if channels has maximum snr to add in tittle
    max_snr_idx = np.argmax(ave_data.get_max_snr_per_channel()[_ch_idx])
    if idx_ch == max_snr_idx:
        title += ' best snr'
    # filter peaks for specific channel
    _peaks, _time_windows = get_channel_peaks_and_windows(eeg_peaks=ave_data.peak_times,
                                                          eeg_time_windows=ave_data.peak_time_windows,
                                                          channel_label=channel_label)
    sorted_peaks = None
    _y_max = None
    _y_min = None
    if _peaks:
        # sort peaks by time
        _s_p = itemgetter(*np.argsort([_p.x for _p in _peaks]))(_peaks)
        sorted_peaks = _s_p if isinstance(_s_p, tuple) else (_s_p,)

        max_amps = [_p.amp for _p in ave_data.peak_times if _p.peak_label]
        if max_amps:
            _y_max = np.max(max_amps)
            _y_min = np.min([_p.amp for _p in ave_data.peak_times if _p.peak_label])

        # compute topographic map
        for _peak in sorted_peaks:
            if _peak.peak_label:
                peak_potentials, max_distance = get_potential_fields(epochs_ave=ave_data,
                                                                     x_val=_peak.x,
                                                                     grid=grid_size,
                                                                     domain=_peak.domain)
                # print _peak.peak_label
                # get_CDF(epochs_ave=ave_data,
                #         x_val=_peak.x,
                #         grid=grid_size,
                #         domain=_peak.domain)
                peak_scalp_potentials.append({'potential': peak_potentials,
                                              'peak': _peak.peak_label})

    # plot topographic map
    inch = 2.54
    fig = plt.figure()
    fig.set_size_inches(14 / inch, 10 / inch)

    row_idx = 0
    gs = gridspec.GridSpec(2, len(peak_scalp_potentials)) if peak_scalp_potentials else gridspec.GridSpec(1, 1)
    if peak_scalp_potentials:
        row_idx += 1
        for _idx, peak_field in enumerate(peak_scalp_potentials):
            ax = plt.subplot(gs[0, _idx])
            ax_im = ax.imshow(peak_field['potential'].T, origin='lower',
                              extent=(-max_distance, max_distance, -max_distance, max_distance),
                              vmin=_y_min,
                              vmax=_y_max,
                              aspect=1.0)
            ax_im.set_cmap('nipy_spectral')
            levels = np.arange(_y_min, _y_max, (_y_max - _y_min) / 5.0)
            ax.contour(peak_field['potential'].T,
                       levels,
                       origin='lower',
                       extent=(-max_distance, max_distance, -max_distance, max_distance),
                       linewidths=1.0,
                       colors='k')
            ax.autoscale(enable=False)
            ax.plot(0, max_distance * 1.0, '|', markersize=5, color='k')
            for i, _ch in enumerate(ave_data.layout):
                if not (_ch.x is None or _ch.y is None):
                    ax.plot(_ch.x, _ch.y, 'o', color='b', markersize=0.2)
            ax.get_xaxis().set_ticks([])
            ax.get_yaxis().set_ticks([])
            ax.axis('off')
            ax.set_title(peak_field['peak'])

        c_bar_ax = fig.add_axes([0.05, 0.6, 0.01, 0.25])
        fig.colorbar(ax_im, cax=c_bar_ax, orientation='vertical', format='%.1f')
        c_bar_ax.yaxis.set_ticks_position('left')

    ax2 = plt.subplot(gs[row_idx, 0:])
    ax2.plot(ave_data.x, ave_data.data[:, idx_ch])
    ax2.axhline(y=ave_data.rn[idx_ch], color='k', linewidth=0.3)
    ax2.axhline(y=-ave_data.rn[idx_ch], color='k', linewidth=0.3)
    if sorted_peaks is not None:
        for _idx, _peak in enumerate(sorted_peaks):
            if _peak.peak_label and _peak.show_label:
                markerfacecolor = 'black' if _peak.significant else 'white'
                if _peak.positive:
                    ax2.plot(_peak.x, _peak.amp, '^', markersize=3, markerfacecolor=markerfacecolor)
                else:
                    ax2.plot(_peak.x, _peak.amp, 'v', markersize=3, markerfacecolor=markerfacecolor)
                ax2.text(_peak.x, _peak.amp * 1.1, _peak.peak_label, horizontalalignment='center',
                         verticalalignment='bottom')
    ax2.set_xlabel('Time [{:}]'.format(ave_data.x_units), fontsize=fontsize)
    ax2.set_xlim(x_lim)
    ax2.set_ylabel('Amplitude [{:}]'.format(ave_data.y_units), fontsize=fontsize)
    if _y_max and _y_min and y_lim is None:
        ax2.set_ylim([_y_min * 1.2, (_y_max + (_y_max - _y_min) / 10.0) * 1.2])
    elif y_lim is not None:
        ax2.set_ylim(y_lim)
    ax2.set_title(title, fontsize=fontsize)
    if _time_windows:
        [ax2.axvspan(_t_w.ini_time, _t_w.end_time, alpha=0.15, color='r')
         for _t_w in _time_windows if _t_w.positive_peak and _t_w.show_window]
        [ax2.axvspan(_t_w.ini_time, _t_w.end_time, alpha=0.15, color='b')
         for _t_w in _time_windows if not _t_w.positive_peak and _t_w.show_window]
    plt.tight_layout()
    fig.savefig(file_name, bbox_inches='tight')
    fig.clf()
    plt.close()
    gc.collect()
    print(('figure saved: ' + file_name))


def plot_time_topographic_map(ave_data=DataNode(),
                              file_name='',
                              title='',
                              times=np.array([]),
                              channel_label: [str] = None,
                              x_lim=None,
                              y_lim=None,
                              fontsize=8):
    if x_lim is None:
        x_lim = [ave_data.x[0], ave_data.x[-1]]
    grid_size = 500j
    peak_scalp_potentials = []

    _ch_idx = [_i for _i, _ch in enumerate(ave_data.layout) if _ch.x is not None and _ch.y is not None]
    # assigned_channels = ave_data.layout[_ch_idx]
    # if channel_label is None:
    #     channel_label = assigned_channels[0].label
    #
    # dummy_var = np.array([_i for _i, _ch in enumerate(assigned_channels) if _ch.label == channel_label])
    # if not len(dummy_var):
    #     return
    idx_ch = ave_data.get_channel_idx_by_label([channel_label])
    if not idx_ch.size:
        return

    # check if channels has maximum snr to add in tittle
    # max_snr_idx = np.argmax(ave_data.get_max_snr_per_channel()[_ch_idx])
    # if idx_ch == max_snr_idx:
    #     title += ' best snr'

    _y_max = None
    _y_min = None
    _time_labels = np.array([''] * times.size)
    _show_labels = np.array([True] * times.size)
    if ave_data.peak_times is not None and ave_data.peak_times.shape[0]:
        _subset = ave_data.peak_times.query('channel == "{:}"'.format(channel_label))
        if _subset.shape[0]:
            # sort peaks by time
            _unique_time_samples, _original_idx = np.unique(ave_data.x_to_samples(_subset['x']), return_index=True)
            _peak_labels = _subset['peak_label'][_original_idx]
            _show = _subset['show_label'][_original_idx]
            times = np.append(times, ave_data.x[_unique_time_samples])
            _time_labels = np.append(_time_labels, _peak_labels)
            _show_labels = np.append(_show_labels, _show)

    if times.size:
        amps = ave_data.data[ave_data.x_to_samples(times), :]
        _y_max = np.nanmax(amps)
        _y_min = np.nanmin(amps)

        # compute topographic map
        max_distance = None
        for _idx_t, _t in enumerate(times):
            peak_potentials, max_distance = get_potential_fields(epochs_ave=ave_data,
                                                                 x_val=_t,
                                                                 grid=grid_size,
                                                                 domain=ave_data.domain)
            if not peak_potentials.size:
                continue
            _label = _time_labels[_idx_t] if _show_labels[_idx_t] else ''
            peak_scalp_potentials.append({'potential': peak_potentials,
                                          'peak': '{:} {:.2e} {:}'.format(_label, _t, ave_data.x_units)}
                                         )

        # plot topographic map
        inch = 2.54
        fig = plt.figure()
        fig.set_size_inches(14 / inch, 10 / inch)

        row_idx = 0
        gs = gridspec.GridSpec(2, len(peak_scalp_potentials)) if peak_scalp_potentials else gridspec.GridSpec(1, 1)
        if peak_scalp_potentials:
            row_idx += 1
            for _idx, peak_field in enumerate(peak_scalp_potentials):
                ax = plt.subplot(gs[0, _idx])
                ax_im = ax.imshow(peak_field['potential'].T, origin='lower',
                                  extent=(-max_distance, max_distance, -max_distance, max_distance),
                                  vmin=_y_min,
                                  vmax=_y_max,
                                  aspect=1.0)
                ax_im.set_cmap('nipy_spectral')
                levels = np.arange(_y_min, _y_max, (_y_max - _y_min) / 5.0)
                ax.contour(peak_field['potential'].T,
                           levels,
                           origin='lower',
                           extent=(-max_distance, max_distance, -max_distance, max_distance),
                           linewidths=1.0,
                           colors='k')
                ax.autoscale(enable=False)
                ax.plot(0, max_distance * 1.0, '|', markersize=5, color='k')
                for i, _ch in enumerate(ave_data.layout):
                    if not (_ch.x is None or _ch.y is None):
                        ax.plot(_ch.x, _ch.y, 'o', color='b', markersize=0.2)
                ax.get_xaxis().set_ticks([])
                ax.get_yaxis().set_ticks([])
                ax.axis('off')
                ax.set_title(peak_field['peak'])

            c_bar_ax = fig.add_axes([0.05, 0.6, 0.01, 0.25])
            fig.colorbar(ax_im, cax=c_bar_ax, orientation='vertical', format='%.1f')
            c_bar_ax.yaxis.set_ticks_position('left')

        ax2 = plt.subplot(gs[row_idx, 0:])
        ax2.plot(ave_data.x, ave_data.data[:, idx_ch])
        ax2.axhline(y=ave_data.rn[idx_ch], color='k', linewidth=0.3)
        ax2.axhline(y=-ave_data.rn[idx_ch], color='k', linewidth=0.3)
        for _t in times:
            ax2.axvline(_t)
        ax2.set_xlabel('Time [{:}]'.format(ave_data.x_units), fontsize=fontsize)
        ax2.set_xlim(x_lim)
        ax2.set_ylabel('Amplitude [{:}]'.format(ave_data.y_units), fontsize=fontsize)
        if _y_max and _y_min and y_lim is None:
            ax2.set_ylim([_y_min * 1.2, (_y_max + (_y_max - _y_min) / 10.0) * 1.2])
        elif y_lim is not None:
            ax2.set_ylim(y_lim)
        ax2.set_title(title, fontsize=fontsize)
        plt.tight_layout()
        fig.savefig(file_name, bbox_inches='tight')
        fig.clf()
        plt.close()
        gc.collect()
        print('figure saved: {:}'.format(file_name))


def plot_freq_topographic_map(ave_data=DataNode(),
                              file_name='',
                              title='',
                              channel_label: [str] = None,
                              x_lim=None,
                              y_lim=None,
                              fontsize=8.0):
    plt.ioff()
    if x_lim is None:
        x_lim = [0, ave_data.x[-1]]
    grid_size = 150j
    peak_scalp_potentials = []

    _ch_idx = [_i for _i, _ch in enumerate(ave_data.layout) if _ch.x is not None and _ch.y is not None]
    # assigned_channels = ave_data.layout[_ch_idx]
    #
    # if channel_label is None:
    #     channel_label = assigned_channels[0].label
    #
    # dummy_var = np.array([_i for _i, _ch in enumerate(assigned_channels) if _ch.label == channel_label])
    # if not len(dummy_var):
    #     return
    idx_ch = ave_data.get_channel_idx_by_label([channel_label])

    # check if channels has maximum snr to add in tittle
    # max_snr_idx = np.argmax(ave_data.get_max_snr_per_channel()[_ch_idx])
    # if idx_ch == max_snr_idx:
    #     title += ' best snr'
    # filter peaks for specific channel
    _peaks, _ = get_channel_peaks_and_windows(eeg_peaks=ave_data.peak_frequency, channel_label=channel_label)

    sorted_peaks = None
    _y_max = None
    max_distance = None
    if _peaks.size:
        # sort peaks by frequency
        sorted_peaks = _peaks.sort_values('x')

        _y_max = ave_data.peak_frequency['amp'].max()

        # compute topographic map
        for _, _peak in sorted_peaks.iterrows():
            if _peak.peak_label:
                peak_potentials, max_distance = get_potential_fields(epochs_ave=ave_data,
                                                                     x_val=_peak.x,
                                                                     grid=grid_size,
                                                                     domain=_peak.domain)
                if peak_potentials.any():
                    peak_scalp_potentials.append({'potential': peak_potentials,
                                                  'peak': _peak.peak_label})

    # plot topographic map
    inch = 2.54
    fig = plt.figure()
    fig.set_size_inches(14 / inch, 10 / inch)
    row_idx = 0
    gs = gridspec.GridSpec(2, len(peak_scalp_potentials)) if peak_scalp_potentials else gridspec.GridSpec(1, 1)

    if peak_scalp_potentials:
        row_idx += 1
        for _idx, peak_field in enumerate(peak_scalp_potentials):
            ax = plt.subplot(gs[0, _idx])
            ax_im = ax.imshow(peak_field['potential'].T, origin='lower',
                              extent=(-max_distance, max_distance, -max_distance, max_distance),
                              vmin=0,
                              vmax=_y_max,
                              aspect=1.0)
            ax_im.set_cmap('nipy_spectral')
            levels = np.arange(0, _y_max, _y_max / 5.0)
            ax.contour(peak_field['potential'].T,
                       levels,
                       origin='lower',
                       extent=(-max_distance, max_distance, -max_distance, max_distance),
                       linewidths=1.0,
                       colors='k')
            ax.autoscale(enable=False)
            ax.plot(0, max_distance * 1.0, '|', markersize=5, color='k')
            for i, _ch in enumerate(ave_data.layout):
                if not (_ch.x is None or _ch.y is None):
                    ax.plot(_ch.x, _ch.y, 'o', color='b', markersize=0.2)
            ax.get_xaxis().set_ticks([])
            ax.get_yaxis().set_ticks([])
            ax.axis('off')
            ax.set_title(peak_field['peak'])

        c_bar_ax = fig.add_axes([0.05, 0.6, 0.01, 0.25])
        fig.colorbar(ax_im, cax=c_bar_ax, orientation='vertical', format='%.1f')
        c_bar_ax.yaxis.set_ticks_position('left')

    # plot waveform of channel used
    ax2 = plt.subplot(gs[row_idx, 0:])
    ax2.plot(ave_data.x, np.abs(ave_data.data[:, idx_ch]))
    if sorted_peaks is not None:
        for _idx, _peak in sorted_peaks.iterrows():
            if _peak.show_label:
                markerfacecolor = 'black' if _peak.significant else 'white'
                if _peak.positive:
                    ax2.plot(_peak.x, _peak.amp, 'v', markersize=3, markerfacecolor=markerfacecolor)
                else:
                    ax2.plot(_peak.x, _peak.amp, '^', markersize=3, markerfacecolor=markerfacecolor)
                if _peak.peak_label:
                    ax2.text(_peak.x, _peak.amp + _y_max / 10.0, _peak.peak_label, horizontalalignment='center',
                             verticalalignment='bottom', fontsize=fontsize)
    ax2.set_xlabel('Frequency [{:}]'.format(ave_data.x_units),
                   fontsize=fontsize)
    ax2.set_xlim(x_lim)
    ax2.set_ylabel('Amplitude [{:}]'.format(ave_data.y_units), fontsize=fontsize)

    if _y_max and y_lim is None:
        ax2.set_ylim([0, _y_max * 1.2])
    elif y_lim is not None:
        ax2.set_ylim(y_lim)
    ax2.set_title(title, fontsize=fontsize)
    fig.savefig(file_name, bbox_inches='tight')
    fig.clf()
    plt.close()
    gc.collect()
    print(('figure saved: ' + file_name))


def plot_eeg_time_frequency_transformation(ave_data: DataNode = DataNode(),
                                           figure_dir_path='',
                                           figure_basename='',
                                           eeg_topographic_map_channels=np.array([]),
                                           title='',
                                           x_lim=None,
                                           y_lim=None,
                                           fig_format='.pdf',
                                           fontsize=8,
                                           **kwargs):
    if eeg_topographic_map_channels.size:
        topographic_map_channels = eeg_topographic_map_channels
    else:
        topographic_map_channels = [_ch.label for _ch in ave_data.layout]

    for _label in topographic_map_channels:
        _fig_path = figure_dir_path + '_' + _label + '_' + figure_basename + '_spectrogram_' + fig_format
        plot_time_frequency_transformation(ave_data=ave_data,
                                           channel_label=_label,
                                           file_name=_fig_path,
                                           title=_label + ' / ' + title,
                                           x_lim=x_lim,
                                           y_lim=y_lim,
                                           fontsize=fontsize,
                                           **kwargs)


def plot_time_frequency_transformation(ave_data: DataNode = DataNode(),
                                       file_name='',
                                       title='',
                                       channel_label: [str] = None,
                                       x_lim=None,
                                       y_lim=None,
                                       fontsize=8,
                                       **kwargs):
    method = kwargs.get('method', 'spectrogram')
    time_window = kwargs.get('time_window', 2.0)
    sample_interval = kwargs.get('sample_interval', 2.0)
    # nperseg = kwargs.get('nperseg', 256)
    # noverlap = kwargs.get('noverlap', 128)
    # nfft = nfft if nperseg < nfft else nperseg
    spec_thresh = kwargs.get('spec_thresh', 4)
    nfft = int(time_window * ave_data.fs)
    noverlap = max(nfft - int(sample_interval * ave_data.fs), 0)

    if x_lim is None:
        x_lim = [0, ave_data.x[-1]]

    _ch_idx = [_i for _i, _ch in enumerate(ave_data.layout) if _ch.x is not None and _ch.y is not None]
    assigned_channels = ave_data.layout[_ch_idx]

    if channel_label is None:
        channel_label = assigned_channels[0].label

    dummy_var = np.array([_i for _i, _ch in enumerate(assigned_channels) if _ch.label == channel_label])
    if not len(dummy_var):
        return
    idx_ch = ave_data.get_channel_idx_by_label([channel_label])

    if not len(idx_ch):
        return
    # check if channels has maximum snr to add in tittle
    # check if channels has maximum snr to add in tittle
    max_snr_idx = np.argmax(ave_data.get_max_snr_per_channel()[_ch_idx])
    if idx_ch == max_snr_idx:
        title += ' best snr'

    # plot topographic map
    inch = 2.54
    fig = plt.figure()
    fig.set_size_inches(14 / inch, 10 / inch)

    row_idx = 0
    gs = gridspec.GridSpec(1, 1)
    ax2 = plt.subplot(gs[row_idx, 0:])
    if method == 'spectrogram':
        freqs, time, power = signal.spectrogram(np.squeeze(ave_data.data[:, idx_ch]),
                                                window=signal.hamming(nfft),
                                                fs=ave_data.fs,
                                                nfft=nfft,
                                                noverlap=noverlap,
                                                mode='magnitude',
                                                scaling='spectrum')
        power /= power.max()
        power = 10 * np.log10(power)
        power[power < -spec_thresh] = -spec_thresh
    if method == 'wavelet':
        freq = np.arange(1, ave_data.data.shape[0] + 1) * ave_data.fs / ave_data.data.shape[0]
        mother = wavelet.Morlet(f0=2 * np.pi)
        s0 = 2 / ave_data.fs  # Starting scale
        dj = 1 / 12.  # Twelve sub-octaves per octaves
        J = 7 / dj  # Seven powers of two with dj sub-octaves
        [cfs, freqs] = pw.cwt(np.squeeze(ave_data.data[:, idx_ch]),
                              scales=np.arange(1, 128),
                              wavelet='cmor',
                              sampling_period=1 / ave_data.fs)

        w = pw.ContinuousWavelet('cmor')
        w.bandwidth_frequency = 1.5
        w.center_frequency = 1
        [cfs, freqs] = pw.cwt(np.squeeze(ave_data.data[:, idx_ch]),
                              scales=np.arange(1, ave_data.data.shape[0] / 2),
                              wavelet=w,
                              sampling_period=1 / ave_data.fs)
        wave, scales, freqs, coi, fft, fftfreqs = wavelet.cwt(np.squeeze(ave_data.data[:, idx_ch]),
                                                              dt=1 / ave_data.fs,
                                                              freqs=freq)
        # power = (np.abs(wave)) ** 2
        # fft_power = np.abs(fft) ** 2
        # period = 1 / freqs
        power = np.abs(cfs)
        time = np.arange(0, power.shape[1]) * 1 / ave_data.fs

    cax = ax2.pcolormesh(time, freqs, power, cmap=plt.get_cmap('viridis'))
    ax2.set_ylabel('Frequency [{:}]'.format(u.Hz))
    ax2.set_xlabel('Time [{:}]'.format(ave_data.x_units), fontsize=fontsize)
    ax2.set_xlim(x_lim)
    if y_lim is not None:
        ax2.set_ylim(y_lim)
    ax2.set_title(title, fontsize=fontsize)
    a_pos = ax2.get_position()
    cbaxes = fig.add_axes([a_pos.x1 + 0.005, a_pos.y0, 0.005, a_pos.height])
    fig.colorbar(cax, cax=cbaxes)

    # plt.tight_layout()
    fig.savefig(file_name, bbox_inches='tight')
    fig.clf()
    plt.close()
    gc.collect()
    print(('figure saved: ' + file_name))


def plot_eeg_time_frequency_power(ave_data: DataNode = DataNode(),
                                  figure_dir_path='',
                                  figure_basename='',
                                  eeg_topographic_map_channels=np.array([]),
                                  title='',
                                  x_lim=None,
                                  y_lim=None,
                                  fig_format='.pdf',
                                  fontsize=8,
                                  **kwargs):
    if eeg_topographic_map_channels.size:
        topographic_map_channels = eeg_topographic_map_channels
    else:
        topographic_map_channels = [_ch.label for _ch in ave_data.layout]

    for _label in topographic_map_channels:
        _fig_path = figure_dir_path + '_' + _label + '_' + figure_basename + '_power_spectrogram_.' + fig_format
        plot_time_frequency_power_transformation(ave_data=ave_data,
                                                 channel_label=_label,
                                                 file_name=_fig_path,
                                                 title=_label + ' / ' + title,
                                                 x_lim=x_lim,
                                                 y_lim=y_lim,
                                                 fontsize=fontsize,
                                                 **kwargs)


def plot_time_frequency_power_transformation(ave_data: DataNode = DataNode(),
                                             time: np.array = np.array([]),
                                             frequency: np.array = np.array([]),
                                             file_name='',
                                             title='',
                                             channel_label: [str] = None,
                                             x_lim=None,
                                             y_lim=None,
                                             fontsize=8,
                                             **kwargs):
    spec_thresh = kwargs.get('spec_thresh', 4)

    if x_lim is None:
        x_lim = [0, time[-1]]

    _ch_idx = [_i for _i, _ch in enumerate(ave_data.layout) if _ch.x is not None and _ch.y is not None]
    assigned_channels = ave_data.layout[_ch_idx]

    if channel_label is None:
        channel_label = assigned_channels[0].label

    dummy_var = np.array([_i for _i, _ch in enumerate(assigned_channels) if _ch.label == channel_label])
    if not len(dummy_var):
        return
    idx_ch = ave_data.get_channel_idx_by_label([channel_label])

    if not len(idx_ch):
        return

    # plot topographic map
    inch = 2.54
    fig = plt.figure()
    fig.set_size_inches(14 / inch, 10 / inch)

    row_idx = 0
    gs = gridspec.GridSpec(1, 1)
    ax2 = plt.subplot(gs[row_idx, 0:])

    power = np.squeeze(np.abs(ave_data.data[:, idx_ch, :]))
    power /= power.max()
    power = 10 * np.log10(power)
    power[power < -spec_thresh] = -spec_thresh
    cax = ax2.pcolormesh(time, frequency, power, cmap=plt.get_cmap('viridis'))
    ax2.set_ylabel('Frequency [{:}]'.format(u.Hz))
    ax2.set_xlabel('Time [{:}]'.format(ave_data.x_units), fontsize=fontsize)
    ax2.set_xlim(x_lim)
    if y_lim is not None:
        ax2.set_ylim(y_lim)
    ax2.set_title(title, fontsize=fontsize)
    a_pos = ax2.get_position()
    cbaxes = fig.add_axes([a_pos.x1 + 0.005, a_pos.y0, 0.005, a_pos.height])
    fig.colorbar(cax, cax=cbaxes)

    # plt.tight_layout()
    fig.savefig(file_name, bbox_inches='tight')
    fig.clf()
    plt.close()
    gc.collect()
    print(('figure saved: ' + file_name))


def plot_eeg_topographic_map(ave_data: DataNode = DataNode(),
                             figure_dir_path='',
                             figure_basename='',
                             eeg_topographic_map_channels=np.array([]),
                             domain=Domain.time,
                             times=np.array([]),
                             title='',
                             x_lim=None,
                             y_lim=None,
                             fig_format='.pdf',
                             fontsize=8):
    eeg_topographic_map_channels = np.array(eeg_topographic_map_channels)

    if eeg_topographic_map_channels.size:
        topographic_map_channels = eeg_topographic_map_channels
    else:
        topographic_map_channels = [_ch.label for _ch in ave_data.layout]

    for _label in topographic_map_channels:
        if domain == Domain.time:
            _fig_path = figure_dir_path + os.sep + _label + '_' + figure_basename + '_TMap' + fig_format
            plot_time_topographic_map(ave_data=ave_data,
                                      channel_label=_label,
                                      file_name=_fig_path,
                                      times=times,
                                      title=_label + ' / ' + title,
                                      x_lim=x_lim,
                                      y_lim=y_lim,
                                      fontsize=fontsize)
        if domain == Domain.frequency:
            _fig_path = figure_dir_path + os.sep + _label + '_' + figure_basename + '_TMap_freq' + fig_format
            plot_freq_topographic_map(ave_data=ave_data,
                                      channel_label=_label,
                                      file_name=_fig_path,
                                      title=_label + ' / ' + title,
                                      x_lim=x_lim,
                                      y_lim=y_lim,
                                      fontsize=fontsize)


def eeg_save_time_slice(data: np.array = np.array([]),
                        fs: np.float = 0,
                        channels: np.array([EegChannel]) = np.array([EegChannel()]),
                        time_length: np.float = 4.0,
                        title: str = '',
                        file_name: str = ''):
    buffer_size = np.floor(np.minimum(time_length * fs, data.shape[0])).astype(np.int)
    fig, ax1 = plt.subplots()
    font = {'size': 6}
    plt.rc('font', **font)
    offset_vector = 10 * np.arange(data.shape[1])
    ax1.plot(np.arange(buffer_size) / fs, data[np.arange(buffer_size), :] - offset_vector,
             linewidth=0.5)
    # if interpolate_data:
    #     ax1.plot(interpolation_data_points['ini'],
    #              np.ones((interpolation_data_points['ini'].shape[0], data.shape[1]))
    #              + np.mean(data[np.arange(buffer_size), :], axis=0) - offset_vector,
    #              marker='v',
    #              linestyle='None',
    #              markerfacecolor='black',
    #              markersize=1.0)
    #
    #     ax1.plot(interpolation_data_points['end'],
    #              np.ones((interpolation_data_points['end'].shape[0], data.shape[1]))
    #              + np.mean(data[np.arange(buffer_size), :], axis=0) - offset_vector,
    #              marker='^',
    #              linestyle='None',
    #              markerfacecolor='black',
    #              markersize=1.0)
    ax1.set_title(title)
    ax1.set_xlabel('Time [s]')
    ax1.set_ylabel('Amplitude [' + r'$\mu$' + 'V]')
    ax2 = ax1.twinx()
    ax2.set_ylim(ax1.get_ylim())
    ax2.set_yticks(np.mean(data[np.arange(buffer_size), :] - offset_vector, axis=0))
    ax2.set_yticklabels([ch.label for i, ch in enumerate(channels)])
    ax2.spines["right"].set_position(("axes", - 0.1))
    fig.savefig(file_name)
    fig.clf()
    plt.close()
    gc.collect()


def get_cdf(epochs_ave: DataNode = DataNode(),
            x_val: np.float = None,
            domain=Domain.time,
            **kwargs):
    idx_elec = np.where(np.array([ch.x for ch in epochs_ave.channels]))[0]
    # remove electrodes without a label
    if domain == Domain.time:
        potentials = epochs_ave.data[epochs_ave.time_to_samples(x_val), idx_elec]
    if domain == Domain.frequency:
        potentials = epochs_ave.rfft_average[epochs_ave.frequency_to_samples(x_val), idx_elec]

    x = np.zeros((potentials.size, 1))
    y = np.zeros((potentials.size, 1))
    z = np.zeros((potentials.size, 1))
    elec_pos = np.zeros((potentials.size, 2))
    _channels = np.array(epochs_ave.channels)[idx_elec]
    for i, _ch in enumerate(_channels):
        if _ch.x is not None and _ch.y is not None:
            x[i] = _ch.x
            y[i] = _ch.y
            z[i] = potentials[i]
            elec_pos[i] = np.array([_ch.x, _ch.y])
    params = {'gdX': 0.05,
              'gdY': 0.05,
              'gdZ': 0.05,
              'n_sources': 64
              }
    elec_pos = get_3D_spherical_positions(x, y)

    params = {'gdX': 0.05,
              'gdY': 0.05,
              'n_sources': 64
              }

    # k = KCSD(elec_pos, z, params)
    #
    # k.estimate_pots()
    # k.estimate_csd()

    # k.plot_all()
    return interpolate_potential_fields(x, y, z, **kwargs)
