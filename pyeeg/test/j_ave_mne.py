__author__ = 'jundurraga-ucl'

import mne
import numpy as np
import processing.weightedAverage as JAve

import matplotlib.pyplot as plt
import scipy

mne.set_log_level('WARNING')
mne.set_log_level('INFO')
mne.set_config('MNE_LOGGING_LEVEL', 'WARNING')
mne_root = '/usr/local/MNE-2.7.0-3106-Linux-x86_64'
fs_home = '/usr/local/freesurfer/'
# subjects_dir = '/home/jundurraga/Documents/Measurements/Biosemi/Test/'
# subjects_ave_dir = '/home/jundurraga/Documents/Measurements/Biosemi/Test/fsaverage/bem/'
# mne.create_default_subject(mne_root=mne_root, fs_home=fs_home,  subjects_dir=subjects_dir, update=True)

# data_path = mne.datasets.sample.data_path()
# subjects_dir = data_path + '/subjects'
# evoked_fname = data_path + '/MEG/sample/sample_audvis-ave.fif'
# trans_fname = data_path + '/MEG/sample/sample_audvis_raw-trans.fif'

# raw_file_name = "/home/jundurraga/Documents/Measurements/Biosemi/Test/NH-IPM-FR-TEST.bdf"
raw_file_name = '/home/jundurraga/Dropbox/Documents/measurements/Rec04.bdf'
raw = mne.io.read_raw_edf(raw_file_name, preload=False)
# mne.io.set_eeg_reference(raw, ['A32'], copy=False)
print((raw.ch_names))
raw.info['bads'] += ['EXG3', 'EXG4', 'EXG5', 'EXG6', 'EXG7', 'EXG8', 'GSR1', 'GSR2', 'Erg1', 'Erg2', 'Resp',
                     'Plet', 'Temp']


picks = mne.pick_types(raw.info, meg=True, eeg=True, eog=True, stim=False, exclude='bads')
# raw.filter(1, 100, method='fft', n_jobs=6)
events = mne.find_events(raw, stim_channel='STI 014')
event_id = dict(auditory=253)  # event trigger and conditions

baseline = (None, 0)
reject = dict(eeg=150000000e-6)
n_samples = np.min(np.diff(events, axis=0), axis=0)[0]
tmin = 0
tmax = (n_samples - 1) / raw.info['sfreq']

epochs = mne.Epochs(raw, events, event_id, tmin, tmax, proj=True, picks=picks, baseline=None, preload=False,
                    reject=reject)

epochs_data = epochs['auditory'].get_data()

j_ave = JAve.JAverager()
j_ave.splits = 34
j_ave.fs = epochs.info['sfreq']
j_ave.t_p_snr = np.arange(0, 1, 0.001) * 1e-3
j_ave.analysis_window = np.array([]) * 1e-3
j_ave.time_offset = 0.0
j_ave.alpha_level = 0.05
j_ave.min_block_size = 10
j_ave.frequencies_to_analyze = np.array([6.78, 41])

for i in range(epochs_data.shape[0]):
    for j in range(epochs_data.shape[1]):
        j_ave.add_sweep(epochs_data[i, j, :])

evoked = epochs['auditory'].average()
