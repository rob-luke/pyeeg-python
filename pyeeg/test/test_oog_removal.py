from pyeeg.processing.tools.epochs_processing_tools import *
import numpy as np
import matplotlib.pyplot as plt
import numpy.matlib

n_ch = 10
n_trials = 80
oog1 = np.sin(2 * np.pi * 10.0 * np.arange(0, 4, 1/16384.0))
w1 = np.random.rand(1, n_ch)
oog1mix = np.matlib.repmat(oog1, n_ch, 1).T * w1
oog2 = np.sin(2 * np.pi * 55.0 * np.arange(0, 4, 1/16384.0))
w2 = np.random.rand(1, n_ch)
oog2mix = np.matlib.repmat(oog2, n_ch, 1).T * w2
ep = np.sin(2 * np.pi * 137.0 * np.arange(0, 4, 1/16384.0))
w3 = np.random.rand(1, n_ch)
epochs = np.matlib.repmat(ep, n_ch, 1).T * w3

data = np.expand_dims(oog1mix + oog2mix + epochs, axis=2) * np.ones((ep.size, n_ch, n_trials))
oog = np.atleast_3d(np.vstack((oog1, oog2)).T) * np.ones((ep.size, 2, n_trials))

clean_data = et_subtract_correlated_ref(epochs=data, ref=oog)
fig = plt.figure()
ax = fig.add_subplot(121)
ax.plot(np.squeeze(data[:, :, 0]))
ax = fig.add_subplot(122)
ax.plot(np.squeeze(clean_data[:, :, 0]))
plt.show()
