import pyeeg.io.edf_bdf_reader as br
import matplotlib.pyplot as plt
import numpy as np
import pyedflib
__author__ = 'jundurraga'

f_name = '/home/jundurraga/Documents/PXXX_JU-20180831T070156Z-001/PXXX_JU/PXXX_JU_Curry/curry_dc/PXXX_JU_C_6_DC.edf'
header = br.read_edf_bdf_header(file_name=f_name)
ch = []
[ch.append(header['channels'][_ch]) for _ch in [0, 1]]
trigger_channel, event_table, sys_code_channel = br.get_trigger_channel(header=header, ini_time=0, end_time=None,
                                                                        trigger_channel_label='EDF Annotations')
data = br.get_data(header=header, channels=ch, ini_time=0, end_time=None)
fs = header['fs'][0]
x = np.arange(0, data.shape[0]) / fs
fig = plt.figure()
plt.plot(x, data - np.mean(data, axis=0))
plt.plot(event_table['idx']/header['fs'][0], np.zeros(event_table['idx'].shape), 'ro')

f = pyedflib.EdfReader(f_name)
n = f.signals_in_file
signal_labels = f.getSignalLabels()
sigbufs = np.zeros((f.getNSamples()[0], n))
for i in np.arange(2):
    sigbufs[:, i] = f.readSignal(i)
xx = np.arange(0, sigbufs.shape[0])/f.getSampleFrequency(0)
plt.plot(xx, sigbufs[:, 0:2] - np.mean(sigbufs[:, 0:2], axis=0))
plt.show()
plt.show()

plt.figure()
f = np.arange(data.shape[0]) * fs / data.shape[0]
plt.plot(f, np.abs(np.fft.fft(data, axis=0)) * 2 / data.shape[0])
plt.xlim(0, 60)
plt.show()
