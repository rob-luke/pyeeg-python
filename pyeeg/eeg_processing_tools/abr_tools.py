import matplotlib.gridspec as gridspec
import pyeeg.plot.eeg_plot_tools as eegpt
import pyeeg.processing.tools.filters.spatial_filtering as sf
from os.path import sep
import pyeeg.definitions.edf_bdf_reader as bioReader
import pyeeg.layouts.layouts as bioLayout
import dataset
import sqlalchemy
import io
import scipy.io
import re
from pyeeg.io.external_tools.aep_gui.aep_matlab_tools import *
from pyeeg.processing.tools.peak_detection.time_domain_tools import *
__author__ = 'jundurraga-ucl'


def get_time_abr(bdf_reader=bioReader.EdfBdfDataReader, **kwargs):
    parameters = kwargs.get('parameters', {})
    channels = kwargs.get('channels', {})
    data_name = kwargs.get('data_name', '')
    path = kwargs.get('path', '')
    save_to_data_base = kwargs.get('save_to_data_base', False)
    data_base_path = kwargs.get('data_base_path', '')
    generate_mat_file = kwargs.get('generate_mat_file', False)
    save_single_recordings_to_db = kwargs.get('save_single_recordings_to_db', False)
    experiment = kwargs.get('experiment', 'abr')
    low_pass = kwargs.get('low_pass', -1)
    high_pass = kwargs.get('high_pass', -1)
    notch_filter = kwargs.get('notch_filter', [50.0, 100.0, 150.0, 200.0, 250.0, 300.0])
    save_components = kwargs.get('save_components', True)
    n_components_plot = kwargs.get('n_components_plot', 10)
    layout = kwargs.get('layout', bioLayout.get_biosemi_64_channels_layout())
    interpolate_data = kwargs.get('interpolate_data', False)
    plot_title_parameters = kwargs.get('plot_title_parameters', [])
    scale = kwargs.get('scale', 1.0e3)
    interpolation_data_points = np.array([])

    if interpolate_data:
        interpolation_data_points = get_interpolation_data_points(**kwargs)

    #  compute steady response for selected frequencies
    j_ave, epochs, n_components, components = get_biosemi_abr(bdf_reader=bdf_reader,
                                                              interpolation_data_points=interpolation_data_points,
                                                              **kwargs)

    peaks = detect_peaks(time_vector=j_ave.time, data_channels=-j_ave.w_average, rn=j_ave.w_rn, snr=j_ave.w_snr)
    # find best snr-channel
    best_chanel = np.argmax(j_ave.w_snr)
    best_ch_peaks = peaks[best_chanel]
    # clasiffy best channels peaks automatically
    classify_abr_peaks([best_ch_peaks])
    # use best channel wave V timing to help label other channels
    wave_v_ref_time = [_peak['time_positive'] for _, _peak in enumerate(best_ch_peaks) if _peak['peak_label'] == 'V']
    if wave_v_ref_time:
        classify_abr_peaks(peaks, max_time=wave_v_ref_time[0])
    else:
        classify_abr_peaks(peaks)

    #  find peaks for averaged channels
    peaks_ave = detect_peaks(time_vector=j_ave.time, data_channels=np.expand_dims(-j_ave.w_average_all_splits(), 1),
                             rn=[j_ave.w_rn_all_splits], snr=[j_ave.w_snr_all_splits])
    classify_abr_peaks(peaks_ave)
    peaks.append(peaks_ave[0])
    dummy_channels = list(channels)
    dummy_channels.append({'label': 'w_average_all_splits', 'number': len(dummy_channels)})

    if generate_mat_file:
        _file_name = kwargs['path'] + sep + data_name + '.mat'
        scipy.io.savemat(_file_name, {'epochs': epochs})

    # get information from parameters
    subject = parameters['Measurement']['MeasurementModule']['Subject']
    measurement_info = {_key: parameters['Measurement']['MeasurementModule'][_key] for _key in
                        ('Comments', 'Condition', 'Date', 'Subject')}
    stimulus_parameters = get_stimulus_parameters(parameters=parameters)

    title_params = []
    for _stimulus in stimulus_parameters:
        title_params.append(['/'] + [item + ':' + '{:10.3f}'.format(_stimulus[item]).strip() +
                                     '/' for i, item in enumerate(plot_title_parameters)])
    _title = measurement_info['Subject']
    for _title_par in title_params:
        _title += '\n'.join([''.join(_title_par)])

    # plot individual channels
    font = {'size': 6}
    offset_step = 200
    offset_vector = np.arange(j_ave.splits + 1) * offset_step
    ave_w = -np.hstack((j_ave.w_average, np.expand_dims(j_ave.w_average_all_splits(), 1))) * scale - offset_vector.reshape(1, -1)
    fig, ax1 = plt.subplots()
    plt.rc('font', **font)
    p1 = ax1.plot(j_ave.time, ave_w, color='g', linewidth=0.5)
    # add detected peaks
    for i, channel_peak in enumerate(peaks):
        for j, _peak in enumerate(channel_peak):
            ax1.plot(_peak['time_positive'], _peak['positive_peak'] * scale - offset_vector[i], '^', markersize=0.5)
            ax1.plot(_peak['time_negative'], _peak['negative_peak'] * scale - offset_vector[i], 'v', markersize=0.5)
            ax1.text(_peak['time_positive'], _peak['positive_peak'] * scale * 1.1 - offset_vector[i], _peak['peak_label'])

    ax1.set_title(_title)
    ax1.set_xlim(0, 0.012)
    ax1.set_ylim(-(j_ave.splits + 1) * offset_step - 1, 1)
    ax1.set_xlabel('Time [s]')
    ax1.set_ylabel('Amplitude [' + 'nV]')
    ax2 = ax1.twinx()
    ax2.set_ylim(ax1.get_ylim())
    ax2.set_yticks(-offset_vector)
    ax2.set_yticklabels([ch['label'] for i, ch in enumerate(dummy_channels)])
    ax2.spines["right"].set_position(("axes", - 0.1))
    plt.legend([p1[0]], ['w_ave'])
    _name_parameters = ('_Hi_' + re.sub('\.', '_', str(kwargs['high_pass'])) + '_Low_' + re.sub('\.', '_', str(kwargs['low_pass'])) +
                        '_SFNC_' + re.sub('\.', '_', str(n_components)))
    fig.savefig(kwargs['path'] + sep + data_name + '_single_channels' +_name_parameters + '.svg', bbox_inches='tight')
    fig.clf()
    if save_components:
        for i in range(np.minimum(n_components_plot, components.shape[1])):
            fig, ax1 = plt.subplots()
            p1 = ax1.plot(j_ave.time, components[:, i], linewidth=0.5)
            ax1.set_title(_title + '/Component:' + str(i))
            ax1.set_xlim(0, 0.012)
            ax1.set_xlabel('Time [s]')
            ax1.set_ylabel('Relative amplitude')
            fig.savefig(kwargs['path'] + sep + data_name +
                        '_single_channels' + _name_parameters +
                        'Component_' + str(i) + '.png', bbox_inches='tight')

    # plot potential fields for highest snr peaks channel time
    grid_size = 150j
    peak_scalp_potentials = []
    for _, _peak in enumerate(peaks_ave[0]):
        if _peak['peak_label'] == 'NA':
            continue
        potential = -j_ave.w_average[_peak['idx_pos'], :] * scale
        peak_potentials, max_distance = eegpt.get_potential_fields(potentials=potential,
                                                                   channels=channels,
                                                                   layout=layout,
                                                                   grid=grid_size)
        peak_scalp_potentials.append({'potential': peak_potentials,
                                      'peak': _peak['peak_label']})

    inch = 2.54
    fig = plt.figure()
    fig.set_size_inches(14/inch, 10 / inch)
    gs = gridspec.GridSpec(2, len(peak_scalp_potentials))
    for _idx, peak_field in enumerate(peak_scalp_potentials):
        ax = plt.subplot(gs[0, _idx])
        ax_im = ax.imshow(peak_field['potential'].T, origin='lower',
                          extent=(-max_distance, max_distance, -max_distance, max_distance),
                          vmin=np.min(peak_field['potential']),
                          vmax=np.max(peak_field['potential']),
                          aspect=1.0)
        ax.autoscale(enable=False)
        channel_labels = [ch['label'] for ch in channels]
        ax.plot(0, max_distance * 1.15, '^', markersize=5, color='w')
        for i, lay in enumerate(layout['label']):
            if lay in channel_labels:
                ax.plot(layout['x'][i], layout['y'][i], 'o', color='b', markersize=0.2)
            else:
                ax.plot(layout['x'][i], layout['y'][i], 'o', color='b', markersize=0.2)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax.axis('off')
        ax.set_title(peak_field['peak'])
        c_bar = fig.colorbar(ax_im, orientation='vertical')
    c_bar.set_label('Amplitude [nV]', fontsize=8)
    ax2 = plt.subplot(gs[1, 0:])
    ax2.plot(j_ave.time, -j_ave.w_average_all_splits() * scale)
    for _idx, _peak in enumerate(peaks[-1]):
        ax2.plot(_peak['time_positive'], _peak['positive_peak'] * scale, '^', markersize=3)
        ax2.plot(_peak['time_negative'], _peak['negative_peak'] * scale, 'v', markersize=3)
        ax2.axhline(y=_peak['rn'] * scale, color='k')
        ax2.axhline(y=-_peak['rn'] * scale, color='k')
        ax2.text(_peak['time_positive'], _peak['positive_peak'] * scale * 1.1, _peak['peak_label'])
    ax2.set_xlabel('Time [s]')
    ax2.set_ylabel('Amplitude [nV]')
    ax2.set_ylim(1.2 * np.min(-j_ave.w_average_all_splits()) * scale, 1.2 * np.max(-j_ave.w_average_all_splits()) * scale)
    ax2.set_title(subject + ' / Channel ' + dummy_channels[-1]['label'])
    plt.tight_layout()
    fig.savefig(path + sep + data_name + '_scalp_dist' + _name_parameters + '_peaks_across_ave' + '.svg',
                bbox_inches='tight')

    if not save_to_data_base:
        return
    #########################################################################
    # add frequency test to data base #######################################
    concatenated_stimuli = cat_dictionary_list(dict_list=stimulus_parameters)
    _data_abr_peaks = generate_peak_dict(channel_peaks=peaks, channels=dummy_channels)
    # connect and add items to data base
    db = dataset.connect('sqlite:///' + data_base_path)
    table_subjects = db['subjects']
    table_subjects.create_column('name', sqlalchemy.String)
    table_subjects.create_column('anonymous_name', sqlalchemy.String)
    # check if subject is in DB
    result = table_subjects.find_one(name=subject)
    if not result:
        _id_subject = table_subjects.insert({'name': subject})
        table_subjects.update(dict(id=_id_subject, anonymous_name='S' + str(_id_subject)), ['id'])
    else:
        print('adding data to subject found in DB')
        _id_subject = result['id']
    # add experiment to table
    table_experiment = db['experiment']
    exp = {'type': experiment, 'id_subject': _id_subject}
    table_experiment.upsert(exp, list(exp.keys()))
    _id_experiment = table_experiment.find_one(type=experiment)['id']

    # add measurement info
    table_measurement_info = db['measurement_info']
    table_measurement_info.create_column('id_experiment', sqlalchemy.INTEGER)
    _row_measurement_info = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject},
                                 **measurement_info)
    table_measurement_info.upsert(_row_measurement_info, list(_row_measurement_info.keys()))
    # add stimuli to table
    table_stimuli = db['stimuli']
    table_stimuli.create_column('id_experiment', sqlalchemy.INTEGER)
    _row_stimuli = dict({'id_experiment': _id_experiment, 'id_subject': _id_subject},
                        **concatenated_stimuli)
    table_stimuli.upsert(_row_stimuli, list(_row_stimuli.keys()))
    _id_stimuli = table_stimuli.find_one(**_row_stimuli)['id']

    # add recording processing to table
    table_recording = db['recording']
    _row_recording = {'id_stimuli': _id_stimuli,
                      'fs': bdf_reader.fs,
                      'system': 'biosemi',
                      'low_pass_filter': low_pass,
                      'high_pass_filter': high_pass,
                      'notch_filter': str(notch_filter),
                      'sf_number_comp': n_components}
    table_recording.upsert(_row_recording, list(_row_recording.keys()))
    _id_recording = table_recording.find_one(**_row_recording)['id']

    # add frequency tests
    table_abr_peaks = db['time_peaks']
    for i, item in enumerate(_data_abr_peaks):
        _row_test = dict({'id_stimuli': _id_stimuli, 'id_recording': _id_recording}, **item)
        table_abr_peaks.upsert(_row_test, list(_row_test.keys()))

    # add time and freq data
    table_responses = db['responses']
    table_responses.create_column('id_stimuli', sqlalchemy.INTEGER)
    table_responses.create_column('id_recording', sqlalchemy.INTEGER)
    table_responses.create_column('domain', sqlalchemy.String)
    table_responses.create_column('type', sqlalchemy.String)
    table_responses.create_column('channel', sqlalchemy.String)
    table_responses.create_column('x', sqlalchemy.LargeBinary)
    table_responses.create_column('y', sqlalchemy.LargeBinary)
    table_responses.create_column('snr_abr', sqlalchemy.Float)
    # save best channels spectra and wave forms to db

    # save individual channels if requested
    if save_single_recordings_to_db:
        for i in range(j_ave.splits):
            data_binary_x = io.BytesIO()
            np.save(data_binary_x, j_ave.time)
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, j_ave.w_average[:, i])
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                              'domain': 'time', 'type': 'w_average',
                              'channel': channels[i]['label'],
                              'x': data_binary_x.read(), 'y': data_binary_y.read(),
                              'snr_abr': j_ave.w_snr[i]}
            table_responses.upsert(_row_time_data, list(_row_time_data.keys()))
        # add across channel weighted average
        data_binary_x = io.BytesIO()
        np.save(data_binary_x, j_ave.time)
        data_binary_y = io.BytesIO()
        np.save(data_binary_y, j_ave.w_average_all_splits())
        data_binary_x.seek(0)
        data_binary_y.seek(0)
        _row_time_data = {'id_stimuli': _id_stimuli, 'id_recording': _id_recording,
                          'domain': 'time', 'type': 'w_average',
                          'channel': 'w_average_all_splits',
                          'x': data_binary_x.read(), 'y': data_binary_y.read(),
                          'snr_abr': j_ave.w_snr_all_splits}
        table_responses.upsert(_row_time_data, list(_row_time_data.keys()))


def get_biosemi_abr(bdf_reader=bioReader.EdfBdfDataReader, triggers={}, channels=[{}], **kwargs):
    data_name = kwargs.get('data_name', '')
    sf_components = kwargs.get('sf_components', np.array([]))
    save_unprocessed_mean = kwargs.get('save_unprocessed_mean', True)
    time_offset = kwargs.get('time_offset', 0)  # in seconds
    analysis_window = kwargs.get('analysis_window', np.array([0, 12]) * 1e-3)  # in seconds

    epochs = bdf_reader.get_epochs(channels=channels, triggers=triggers, **kwargs)

    # trunc epochs to analysis window
    if analysis_window.size > 0:
        ini_sample = np.round((analysis_window[0] + time_offset) * bdf_reader.fs).astype(np.int)
        end_sample = np.round((analysis_window[1] + time_offset) * bdf_reader.fs).astype(np.int)
        epochs = epochs[ini_sample: end_sample, :, :]

    mean_data = np.mean(epochs, axis=2)

    if save_unprocessed_mean:
        fig, ax1 = plt.subplots()
        font = {'size': 6}
        plt.rc('font', **font)
        offset_vector = 0.1 * np.arange(mean_data.shape[1])
        ax1.plot(np.arange(mean_data.shape[0]) / bdf_reader.fs, mean_data - offset_vector, linewidth=0.5)
        _title = 'unprocessed_data'
        ax1.set_title(_title)
        ax1.set_xlabel('Time [s]')
        ax1.set_ylabel('Amplitude [' + r'$\mu$' + 'V]')
        ax2 = ax1.twinx()
        ax2.set_ylim(ax1.get_ylim())
        ax2.set_yticks(-offset_vector)
        ax2.set_yticklabels([ch['label'] for i, ch in enumerate(channels)])
        ax2.spines["right"].set_position(("axes", - 0.1))
        fig.savefig(kwargs['path'] + sep + data_name +'_single_channels_unprocessed_epochs' + '.svg', bbox_inches='tight')
        fig.clf()

    # ica = FastICA(n_components=3)
    # epochs_= ica.fit_transform(epochs)

    c0 = sf.nt_covariance(epochs)
    c1 = sf.nt_covariance(np.expand_dims(mean_data, 2))

    todss, pwr0, pwr1 = sf.nt_dss0(c0, c1)
    if not sf_components.any():
        p_ratio = pwr1 / pwr0
        pos = np.maximum(np.where(np.cumsum(p_ratio)/np.sum(p_ratio) >= 0.8)[0][0], 1)
        n_components = np.arange(pos)
    else:
        n_components = sf_components

    z = sf.nt_mmat(epochs, todss)
    cov_1 = sf.nt_x_covariance(z, epochs)
    epochs = sf.nt_mmat(z[:, n_components, :], cov_1[n_components, :])

    j_ave = JAve.JAverager()
    j_ave.splits = epochs.shape[1]
    j_ave.time_offset = 0
    j_ave.fs = bdf_reader.fs
    j_ave.t_p_snr = np.arange(0, epochs.shape[0] / bdf_reader.fs, epochs.shape[0]/10.0 / bdf_reader.fs)
    j_ave.analysis_window = analysis_window
    j_ave.fft_analysis = False
    j_ave.alpha_level = 0.05
    j_ave.min_block_size = 32
    j_ave.plot_sweeps = False

    _n_sweep = 0
    _total_sweeps = epochs.shape[1] * epochs.shape[2]
    for i in range(epochs.shape[2]):
        for j in range(epochs.shape[1]):
            j_ave.add_sweep(epochs[:, j, i])
            _n_sweep += 1
            print(('current sweep:' + str(_n_sweep) + '/' + str(_total_sweeps)))
    return j_ave, epochs, len(n_components), np.mean(z, axis=2)
