#! /usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
setup(name="pyeeg",
      version="0.0.1",
      packages=find_packages(),
      author="Jaime Undurraga",
      author_email="jaime.undurraga@gmail.com;",
      description="EEG tools for reading and analysis.",
      long_description=\
      """
      Set of tools for processing data using bdf/edf eeg file format
      """,
      license="GPL v3",
      url="",
      package_data={'': ['*.lay']},
      include_package_data=True,
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python :: 3',
          'Topic :: Scientific/Engineering :: Bio-Informatics'
          ]
      )
