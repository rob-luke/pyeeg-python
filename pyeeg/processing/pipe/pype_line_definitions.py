from pyeeg.processing.pipe.definitions import InputOutputProcess
import pyeeg.processing.tools.filters.eegFiltering as eegf
from pyeeg.processing.tools.multiprocessing.multiprocessesing_filter import filt_filt_data
from pyeeg.processing.tools.epochs_processing_tools import *
from pyeeg.processing.tools.eeg_epoch_operators import *
from pyeeg.processing.tools.peak_detection.time_domain_tools import detect_peaks_and_amplitudes, TimePeakWindow, \
    PeakToPeakMeasure
from pyeeg.processing.tools.roi.definitions import TimeROI
from pyeeg.definitions.eeg_definitions import EegPeak
from pyeeg.plot import eeg_ave_epochs_plot_tools as eegpt
from pyeeg.definitions.channel_definitions import Domain
from pyeeg.definitions.events import SingleEvent, Events
from pyeeg.processing.statistics.definitions import *
from sklearn.linear_model import LinearRegression
from pyeeg.processing.statistics.eeg_statistic_tools import hotelling_t_square_test
import numpy as np
import astropy.units as u
import pandas as pd
from sklearn.decomposition import FastICA
from sklearn.cross_decomposition import CCA
import itertools
from scipy.signal import find_peaks, windows
from scipy.stats import f
import matplotlib.gridspec as gridspec
import os
import copy
from matplotlib.ticker import FormatStrFormatter
from pyeeg.processing.tools.multiprocessing.multiprocessesing_filter import filt_data
from pyeeg.definitions.channel_definitions import ChannelItem


class PipeItem:
    def __init__(self, name='', process: InputOutputProcess = None):
        self.name = name
        self.process = process


class PipePool(list):
    """Class to implement an iterator
    of powers of two"""

    def __init__(self):
        super(PipePool, self).__init__()
        self._pool = []

    def append(self, item: object, name=None):
        if name is None:
            name = type(item).__name__
        super(PipePool, self).append(PipeItem(**{'name': name, 'process': item}))

    def __getitem__(self, key):
        return super(PipePool, self).__getitem__(key)

    def get_process(self, value):
        out = None
        for _i, _item in enumerate(self):
            if _item.name == value:
                out = _item.process
                break
        return out


class ReSampling(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, new_sampling_rate=None, **kwargs):
        super(ReSampling, self).__init__(input_process=input_process, **kwargs)
        self.new_sampling_rate = new_sampling_rate

    def transform_data(self):
        data, _factor = eegf.eeg_resampling(x=self.input_node.data,
                                            factor=self.new_sampling_rate / self.input_node.fs)
        self.output_node.data = data
        self.output_node.fs = self.input_node.fs * _factor
        self.output_node.process_history.append(self.process_parameters)


class ReferenceData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, reference_channels: [str] = None, remove_reference=True,
                 invert_polarity=False, **kwargs):
        super(ReferenceData, self).__init__(input_process=input_process, **kwargs)
        if reference_channels is None:
            reference_channels = ['']
        self.reference_channels = reference_channels
        self.remove_reference = remove_reference
        self.invert_polarity = invert_polarity

    def transform_data(self):
        _ch_idx = self.input_node.get_channel_idx_by_label(self.reference_channels)
        if not _ch_idx.size:
            self.remove_reference = False
            _ch_idx = np.arange(self.input_node.layout.size)
        print('Referencing data to: ' + ''.join(['{:s} '.format(_ch.label) for _ch in self.input_node.layout[_ch_idx]]))
        reference = np.mean(self.input_node.data[:, _ch_idx], axis=1, keepdims=True)
        self.output_node.data = (self.input_node.data - reference) * (-1.0) ** self.invert_polarity

        if self.remove_reference:
            self.output_node.delete_channel_by_idx(_ch_idx)


class FilterData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, high_pass=None, low_pass=None, **kwargs):
        """
        Filter EEG data using a zero group-delay technique
        :param input_process: InputOutputProcess Class
        :param high_pass: Frequency (in Hz) of high-pass filter
        :param low_pass: Frequency (in Hz) of low-pass filter
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(FilterData, self).__init__(input_process=input_process, **kwargs)
        self.low_pass = low_pass
        self.high_pass = high_pass

    def transform_data(self):
        if not self.low_pass and not self.high_pass:
            self.output_node.data = self.input_node.data
            return
        filtered_signal = self.input_node.data.copy()
        _b = eegf.bandpass_fir_win(high_pass=self.high_pass, low_pass=self.low_pass, fs=self.input_node.fs)
        filtered_signal = filt_filt_data(input_data=filtered_signal, b=_b)
        self.output_node.data = filtered_signal


class HilbertEnvelope(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, high_pass=None, low_pass=None, **kwargs):
        """
        This process computes the  Hilbert Envelope of EEG data
        :param input_process: InputOutputProcess Class
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(HilbertEnvelope, self).__init__(input_process=input_process, **kwargs)

    def transform_data(self):
        data = self.input_node.data.copy()
        _fft = pyfftw.builders.fft(data, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
                                   threads=multiprocessing.cpu_count())
        fx = _fft()
        n = fx.shape[0]
        h = np.zeros(n)
        if n % 2 == 0:
            h[0] = h[n // 2] = 1
            h[1:n // 2] = 2
        else:
            h[0] = 1
            h[1:(n + 1) // 2] = 2
        _ifft = pyfftw.builders.ifft(fx * h.reshape(-1, 1), overwrite_input=False, planner_effort='FFTW_ESTIMATE',
                                     axis=0,
                                     threads=multiprocessing.cpu_count())
        hilbert_data = _ifft()
        self.output_node.data = np.abs(hilbert_data)


class RegressOutEOG(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, ref_channel_labels: [str] = None, high_pass=0.3, low_pass=7.0,
                 peak_width=0.1,  # seconds
                 peak_quantile=0.95,  # quantile to define peak prominance
                 template_width=1.0,
                 remove_eog_channels=True,
                 save_figure=True,  # save figure
                 fig_format='.png',
                 **kwargs):
        """
        This class removes EOG artifacts using a template technique. Blinking artifacts are detected and averaged to
        generate a template. This template is scaled for each channel in order to maximize correlation between each
        individual blink and individual events on each channel
        The resulting template is removed from the data, reducing blinks artifacts.
        :param input_process: InputOutputProcess Class
        :param ref_channel_labels: a list with the channel labels that contain the EOG
        :param high_pass: Frequency (in Hz) of high-pass filter all data. This is necessary to generate the template
        :param low_pass: Frequency (in Hz) of low-pass filter only applied to EOG channels
        :param peak_width: default minimum width (in seconds) to detect peaks
        :param peak_quantile: between 0 and 1, it determines the threshold to detect peaks
        :param template_width: the duration (in seconds) of the time window to average and generate a template
        :param remove_eog_channels: if true EOG channels will be removed once data has been cleaned
        :param save_figure: whether to save or not figures showing the detection and removal of blinks
        :param fig_format: format of output figure
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RegressOutEOG, self).__init__(input_process=input_process, **kwargs)
        self.ref_channel_labels = ref_channel_labels
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.peak_width = peak_width
        self.peak_quantile = peak_quantile
        self.template_width = template_width
        self.remove_eog_channels = remove_eog_channels
        self.save_figure = save_figure
        self.fig_format = fig_format

    def transform_data(self):
        template_samples = int(self.template_width * self.input_node.fs)
        data = self.input_node.data.copy()
        _ref_idx = self.input_node.get_channel_idx_by_label(labels=self.ref_channel_labels)
        if _ref_idx.size:
            # demean data before filtering to minimize ringing filter effects
            data -= np.mean(data, axis=0)
            # filter
            _b = eegf.bandpass_fir_win(high_pass=self.high_pass, low_pass=None, fs=self.input_node.fs)
            data = filt_filt_data(input_data=data, b=_b)
            _cov = data[:, _ref_idx]
            # low pass eye artifacts
            _b = eegf.bandpass_fir_win(high_pass=None, low_pass=self.low_pass, fs=self.input_node.fs)
            _cov = filt_filt_data(input_data=_cov, b=_b)

            if self.save_figure:
                figure_dir_path = self.input_node.paths.figures_current_dir
                figure_basename = self.__class__.__name__
                inch = 2.54
                fig = plt.figure()
                fig.set_size_inches(14 / inch, 10 / inch)
                gs = gridspec.GridSpec(2, 1)

            # find propagation coefficients and subtract artifacts
            for _idx_coef in range(_cov.shape[1]):
                ref = _cov[:, _idx_coef]
                # find peaks and generate template
                q = [np.quantile(ref, self.peak_quantile), np.quantile(ref, 1 - self.peak_quantile)]
                ref_amp = q[np.argmax(np.abs(q))]
                _idx_peak, _amp = find_peaks((-1) ** (ref_amp < 0) * ref,
                                             width=int(self.input_node.fs * self.peak_width), height=np.abs(ref_amp))
                template = np.zeros((template_samples, 1))
                reconstructed = np.zeros(data.shape)
                if _idx_peak.size:
                    for _peak in _idx_peak:
                        _ini = _peak - template_samples // 2
                        _end = _peak + template_samples // 2
                        if _ini < 0:
                            template[-_ini::, 0] += ref[0: _end]
                        elif _end > data.shape[0]:
                            template[0:-(_end - data.shape[0]):, 0] += ref[_ini::]
                        else:
                            template[:, 0] += ref[_ini: _end]
                    template /= _idx_peak.size
                    template *= np.atleast_2d(windows.blackman(template_samples)).T
                    # we recreate a peak template
                    for _peak in _idx_peak:
                        _current_template = template
                        _ini = _peak - template_samples // 2
                        _end = _peak + template_samples // 2
                        if _ini < 0:
                            _current_template = template[-_ini::, :]
                        if _end > data.shape[0]:
                            _current_template = template[0:-(_end - data.shape[0]), :]

                        _ini = max(_peak - template_samples // 2, 0)
                        _end = min(_peak + template_samples // 2, data.shape[0])

                        original = data[_ini: _end, :]
                        transmission_index = original.T.dot(_current_template) / np.expand_dims(
                            np.sum(np.square(_current_template)), axis=0)
                        reconstructed[_ini: _end, :] = np.repeat(
                            _current_template,
                            data.shape[1],
                            axis=1) * transmission_index.T
                    if self.save_figure:
                        ax = plt.subplot(gs[0, :])
                        ax.plot(_cov[:, _idx_coef] + _idx_coef * _cov.max(), label='original reference')
                        ax.plot(_idx_peak, _cov[_idx_peak, _idx_coef] + _idx_coef * _cov.max(), 'o',
                                label='original reference')
                        ax.plot(reconstructed[:, _ref_idx[_idx_coef]] + _idx_coef * _cov.max(),
                                label='fitted artifacts')
                        ax.legend()
                # remove reconstructed oeg activity (first iteration may remove most of them)
                data -= reconstructed
            if self.save_figure:
                ax = plt.subplot(gs[1, :])
                ax.plot(_cov + np.arange(_cov.shape[1]) * _cov.max(), label='oeg artifacts')
                ax.plot(data[:, _ref_idx] + np.arange(_cov.shape[1]) * _cov.max(), label='cleaned artifacts')
                ax.legend()
                _fig_path = figure_dir_path + os.sep + '_' + figure_basename + '_eog' + self.fig_format
                fig.tight_layout()
                fig.savefig(_fig_path)
                fig.clf()

        self.output_node.data = data
        if self.remove_eog_channels and _ref_idx.size:
            self.output_node.delete_channel_by_idx(_ref_idx)


class RegressOutICA(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, ref_channel_labels: [str] = None, **kwargs):
        super(RegressOutICA, self).__init__(input_process=input_process, **kwargs)
        self.ref_channel_labels = ref_channel_labels

    def transform_data(self):
        data = self.input_node.data.copy()
        _ref_idx = self.input_node.get_channel_idx_by_label(labels=self.ref_channel_labels)
        _cov = data[:, _ref_idx]
        _offset = int(data.shape[0] * 0.10)

        # Compute ICA
        print('Performing ICA analysis')
        ica = FastICA(n_components=data.shape[1], max_iter=1000)
        ica.fit(data)
        # decompose signal into components
        components = ica.fit_transform(data)
        corr_coefs = np.empty((components.shape[1], _cov.shape[1]))
        for _i_com, _i_cov in itertools.product(range(components.shape[1]), range(_cov.shape[1])):
            corr_coefs[_i_com, _i_cov] = \
                np.corrcoef(_cov[_offset:-_offset, _i_cov], components[_offset:-_offset, _i_com])[0, 1]
        _idx_to_remove = np.argmax(np.abs(corr_coefs), axis=0)
        print('Maximum correlations: {:}'.format(corr_coefs[np.argmax(np.abs(corr_coefs), axis=0), :]))
        print('Removing components: {:}'.format(_idx_to_remove))
        components[:, _idx_to_remove] = 0
        clean_data = ica.inverse_transform(components)
        self.output_node.data = clean_data


class RegressOutCCA(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, ref_channel_labels: [str] = None, **kwargs):
        super(RegressOutCCA, self).__init__(input_process=input_process, **kwargs)
        self.channel_labels = ref_channel_labels

    def transform_data(self):
        data = self.input_node.data.copy()
        _ref_idx = self.input_node.get_channel_idx_by_label(labels=self.ref_channel_labels)
        _cov = data[:, _ref_idx]
        _offset = int(data.shape[0] * 0.10)

        # Compute ICA
        print('Performing ICA analysis')

        cca = CCA(n_components=data.shape[1])
        cca.fit(data, _cov)
        # decompose signal into components
        X_c, Y_c = cca.transform(data, _cov)
        # corr_coefs = np.empty((components.shape[1], _cov.shape[1]))
        # for _i_com, _i_cov in itertools.product(range(components.shape[1]), range(_cov.shape[1])):
        #     corr_coefs[_i_com, _i_cov] = \
        #     np.corrcoef(_cov[_offset:-_offset, _i_cov], components[_offset:-_offset, _i_com])[0, 1]
        # _idx_to_remove = np.argmax(np.abs(corr_coefs), axis=0)
        # print('Maximum correlations: {:}'.format(corr_coefs[np.argmax(np.abs(corr_coefs), axis=0), :]))
        # print('Removing components: {:}'.format(_idx_to_remove))
        # components[:, _idx_to_remove] = 0
        # clean_data = ica.inverse_transform(components)

        # self.output_node.data = clean_data


class EpochData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 pre_stimulus_interval=None,
                 post_stimulus_interval=None,
                 event_code: float = None,
                 base_line_correction: bool = False,
                 de_trend: bool = True,
                 **kwargs):
        """
        This class will take a n*m matrix into a k*m*p, where p (number of trials) is determined by the number of
        triggers used to split the data
        :param input_process: input_process: InputOutputProcess Class
        :param pre_stimulus_interval: the length (in sec) of the data to be read before the trigger
        :param post_stimulus_interval: the length (in sec) of the data to be read after the trigger
        :param event_code: integer indicating the event code to be used to epoch the data
        :param base_line_correction: whether to perform beaseline correction or not
        :param de_trend: whether to remove linear components from each epoch
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(EpochData, self).__init__(input_process=input_process, **kwargs)
        self.pre_stimulus_interval = pre_stimulus_interval
        self.post_stimulus_interval = post_stimulus_interval
        self.event_code = event_code
        self.base_line_correction = base_line_correction
        self.de_trend = de_trend

    def transform_data(self):
        if self.event_code is None:
            print('no event code was provided')
            return
        pre_stimulus_interval = 0.0 if self.pre_stimulus_interval is None else self.pre_stimulus_interval
        events_index = self.input_node.events.get_events_index(code=self.event_code, fs=self.input_node.fs)
        post_stimulus_interval = self.post_stimulus_interval
        if self.post_stimulus_interval is None:
            post_stimulus_interval = np.percentile(np.diff(events_index), 90) / self.input_node.fs
        buffer_size = np.int((post_stimulus_interval + pre_stimulus_interval) * self.input_node.fs)
        events_index = events_index - int(pre_stimulus_interval * self.input_node.fs)
        events_index = events_index[events_index > 0]
        epochs = np.zeros((buffer_size, self.input_node.data.shape[1], events_index.size), dtype=np.float32)
        for i, _event in enumerate(events_index):
            # ensure that blocks match buffer size
            if _event + buffer_size > self.input_node.data.shape[0]:
                epochs = epochs[:, :, list(range(i))]
                break
            epochs[:, :, i] = self.input_node.data[_event:_event + buffer_size, :]
        print('A total of {:} epochs were obtained using event code {:}, each with a duration of {:.3f} s'.format(
            epochs.shape[2],
            self.event_code,
            buffer_size / self.input_node.fs))
        if self.de_trend:
            epochs -= np.mean(epochs, axis=0)
        if self.base_line_correction:
            _ini_sample = 0
            _end_sample = max(1, int(pre_stimulus_interval * self.input_node.fs))
            epochs -= np.mean(epochs[_ini_sample:_end_sample, :, :], axis=0)
        self.output_node.data = epochs
        self.output_node.time_offset = pre_stimulus_interval


class RejectEpochs(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 method: str = 'std',
                 std_threshold: float = 3.0,
                 threshold: float = 300.0,
                 percentage: float = 0.1,
                 max_epochs_above_threshold: float = 0.5,
                 **kwargs):
        """
        This class will epochs where a given threshold have been exceeded. If any channel exceed the threshold at a
        given trial, that particular epoch will be removed from all channels. This is done to keep the data in a single
        matrix.
        :param input_process: input_process: InputOutputProcess Class
        :param method: string indicating if rejection will be based on standard deviation "std" or threhsold "threshold"
        :param std_threshold: float indicating the threshold standard deviation to remove epochs
        :param threshold: the threshold to reject channels with too much noise
        :param max_epochs_above_threshold: if a single channel has more than max_epochs_above_threshold (percentage)
        epochs, the channel will be removed.
        :param percentage indicates the percentage of epochs to remove.
        above the threshold, the channel will be removed.
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RejectEpochs, self).__init__(input_process=input_process, **kwargs)
        self.method = method
        self.std_threshold = std_threshold
        self.threshold = threshold
        self.max_epoch_above_threshold = max_epochs_above_threshold
        self.percentage = percentage

    def transform_data(self):
        _data = self.input_node.data
        if self.method == "threshold":
            _max_amp = np.max(np.abs(_data), axis=0)
            _epoch_limit = int(self.max_epoch_above_threshold * _data.shape[2])
            _n_noisy_epochs_per_channel = np.sum(_max_amp > self.threshold, axis=1)
            _ch_to_remove = np.argwhere(_n_noisy_epochs_per_channel > _epoch_limit).flatten()
            print('Removing a total of {:} channels with more than {:} epochs above the threshold {:}'.format(
                np.sum(_ch_to_remove.size), _epoch_limit, self.threshold))
            # we assign data to output node to perform a clean delete of bad channels
            self.output_node.data = _data
            self.output_node.delete_channel_by_idx(_ch_to_remove)
            # now using the new data we find and remove the x % of ephocs with the highest peak
            _data = self.output_node.data
            _max_amp = np.max(np.abs(_data), axis=0)
            rejection_thr = np.quantile(np.max(_max_amp, axis=0), 1 - self.percentage)
            _idx_to_keep = np.argwhere(np.max(_max_amp, axis=0) < rejection_thr).flatten()
        if self.method == "std":
            _epochs_std = np.std(_data, axis=0)
            _n_noisy_epochs_across_channels = np.sum(np.greater(_epochs_std,
                                                                (np.mean(_epochs_std, 1) +
                                                                 self.std_threshold * np.std(_epochs_std, 1)).reshape(
                                                                    -1, 1)),
                                                     axis=0)
            _idx_to_keep = np.argwhere(_n_noisy_epochs_across_channels == 0).flatten()
        print('Rejecting a total of {:} epochs, corresponding to {:} % of the total number of epochs'.format(
            _epochs_std.shape[1] - _idx_to_keep.size,
            (_epochs_std.shape[1] - _idx_to_keep.size) * 100 / _epochs_std.shape[1]))
        self.output_node.data = _data[:, :, _idx_to_keep]


class RemoveBadChannelsEpochsBased(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 threshold: float = 300.0,
                 max_epochs_above_threshold: float = 0.5,
                 **kwargs):
        """
        This class will remove a channel based on epochs data. If any channel has more than max_epochs_above_threshold
        percentage of epochs above a given threshold, this channel will be removed.
        :param input_process: input_process: InputOutputProcess Class
        :param threshold: the threshold to reject channels with too much noise
        :param max_epochs_above_threshold: if a single channel has more than max_epochs_above_threshold (percentage)
        epochs, the channel will be removed.
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RemoveBadChannelsEpochsBased, self).__init__(input_process=input_process, **kwargs)
        self.threshold = threshold
        self.max_epoch_above_threshold = max_epochs_above_threshold

    def transform_data(self):
        _data = self.input_node.data
        _max_amp = np.max(np.abs(_data), axis=0)
        _epoch_limit = int(self.max_epoch_above_threshold * _data.shape[2])
        _n_noisy_epochs_per_channel = np.sum(_max_amp > self.threshold, axis=1)
        _ch_to_remove = np.argwhere(_n_noisy_epochs_per_channel > _epoch_limit).flatten()
        print('Removing a total of {:} channels with more than {:} epochs above the threshold {:}'.format(
            np.sum(_ch_to_remove.size), _epoch_limit, self.threshold))
        # we assign data to output node to perform a clean delete of bad channels
        self.output_node.data = _data
        self.output_node.delete_channel_by_idx(_ch_to_remove)


class SortEpochs(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 ascending=False,
                 sort_by: str = 'std',
                 **kwargs):
        """
        This class will sort epochs by maximum amplitude across all trials.
        :param input_process: input_process: InputOutputProcess Class
        :param sort_by: string indicating the method to sort ephocs, either 'std' for standard deviation or 'amp' to use
        maximum amplitude per epoch.
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(SortEpochs, self).__init__(input_process=input_process, **kwargs)
        self.ascending = ascending
        self.sort_by = sort_by

    def transform_data(self):
        _data = self.input_node.data
        if self.sort_by == 'amp':
            _measured_value = np.max(np.abs(_data), axis=0)
            _idx_sorted = np.argsort(_measured_value, axis=1)
        if self.sort_by == 'std':
            _measured_value = np.std(_data, axis=0)
            _idx_sorted = np.argsort(_measured_value, axis=1)
        sorted_data = np.zeros(_data.shape)
        if self.ascending:
            for _ch in range(_idx_sorted.shape[0]):
                sorted_data[:, _ch, :] = _data[:, _ch, _idx_sorted[_ch, :][::-1]]
        else:
            for _ch in range(_idx_sorted.shape[0]):
                sorted_data[:, _ch, :] = _data[:, _ch, _idx_sorted[_ch, :]]

        print('Sorting epochs by {:}'.format(self.sort_by))
        self.output_node.data = sorted_data


class SpatialFilter(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, sf_join_frequencies=None, **kwargs):
        super(SpatialFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_join_frequencies = sf_join_frequencies
        self.pwr0 = None
        self.pwr1 = None
        self.cov = None

    def transform_data(self):
        # compute spatial filter
        z, pwr0, pwr1, cov = et_get_spatial_filtering(epochs=self.input_node.data,
                                                      fs=self.input_node.fs,
                                                      sf_join_frequencies=self.sf_join_frequencies)
        self.output_node.data = z
        self.pwr0 = pwr0
        self.pwr1 = pwr1
        self.cov = cov


class ApplySpatialFilter(InputOutputProcess):
    def __init__(self, input_process: SpatialFilter = None, sf_components=np.array([]),
                 sf_thr=0.8,
                 **kwargs):
        super(ApplySpatialFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_components = sf_components
        self.sf_thr = sf_thr

    def transform_data(self):
        filtered_data, _ = et_apply_spatial_filtering(z=self.input_node.data,
                                                      pwr0=self.input_process.pwr0,
                                                      pwr1=self.input_process.pwr1,
                                                      cov_1=self.input_process.cov,
                                                      sf_components=self.sf_components,
                                                      sf_thr=self.sf_thr)
        self.output_node.data = filtered_data


class CreateAndApplySpatialFilter(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 sf_join_frequencies=None,
                 sf_components=np.array([]),
                 sf_thr=0.8,
                 components_to_plot: np.array = np.arange(0, 10),
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 user_naming_rule: str = '',
                 **kwargs):
        super(CreateAndApplySpatialFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_join_frequencies = sf_join_frequencies
        self.sf_components = sf_components
        self.sf_thr = sf_thr
        self.components_to_plot = components_to_plot
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.user_naming_rule = user_naming_rule
        self.__kwargs = kwargs

    def transform_data(self):
        spatial_filter = SpatialFilter(input_process=self.input_process,
                                       sf_join_frequencies=self.sf_join_frequencies)
        spatial_filter.run()
        filtered_data = ApplySpatialFilter(input_process=spatial_filter,
                                           sf_components=self.sf_components,
                                           sf_thr=self.sf_thr,
                                           **self.__kwargs)
        filtered_data.run()
        self.output_node = filtered_data.output_node
        if self.components_to_plot is not None:
            plotter = PlotSpatialFilterComponents(spatial_filter,
                                                  plot_x_lim=self.plot_x_lim,
                                                  plot_y_lim=self.plot_y_lim,
                                                  user_naming_rule=self.user_naming_rule
                                                  )
            plotter.run()
            plotter = ProjectSpatialComponents(spatial_filter,
                                               user_naming_rule=self.user_naming_rule,
                                               plot_x_lim=self.plot_x_lim,
                                               plot_y_lim=self.plot_y_lim
                                               )
            plotter.run()


class PlotSpatialFilterComponents(InputOutputProcess):
    def __init__(self, input_process=SpatialFilter,
                 components_to_plot: np.array = np.arange(0, 10),
                 user_naming_rule: str = '_components_',
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 **kwargs):
        super(PlotSpatialFilterComponents, self).__init__(input_process=input_process, **kwargs)
        self.components_to_plot = components_to_plot
        self.user_naming_rule = user_naming_rule
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.__kwargs = kwargs

    def transform_data(self):
        if self.components_to_plot is not None:
            # we copy the input process to overried the layout without affecting other pipe processes
            _input_process = copy.deepcopy(self.input_process)
            average = AverageEpochs(_input_process)
            average.run()
            # we copy replace the layout with a copy to avid to overwrite the parent layout
            for _i, _lay in enumerate(average.output_node.layout):
                _lay.label = str(_i)
            plotter = PlotTimeWaveforms(average,
                                        user_naming_rule=self.user_naming_rule,
                                        idx_ch_to_plot=self.components_to_plot,
                                        plot_x_lim=self.plot_x_lim,
                                        plot_y_lim=self.plot_y_lim)
            plotter.run()


class ProjectSpatialComponents(InputOutputProcess):
    def __init__(self, input_process=SpatialFilter,
                 components_to_plot: np.array = np.arange(0, 10),
                 user_naming_rule: str = '',
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 **kwargs):
        super(ProjectSpatialComponents, self).__init__(input_process=input_process, **kwargs)
        self.components_to_plot = components_to_plot
        self.user_naming_rule = user_naming_rule
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.__kwargs = kwargs

    def transform_data(self):
        for _component in self.components_to_plot:
            projected_component = ApplySpatialFilter(input_process=self.input_process,
                                                     sf_components=np.array([_component]))
            projected_component.run()
            average = AverageEpochs(projected_component)
            average.run()

            max_chan = np.argmax(np.std(average.output_node.data, axis=0))
            cha_max_pow_label = average.output_node.layout[max_chan].label
            # find max and min within plot_x_lim
            if self.plot_x_lim is not None:
                _samples = average.output_node.x_to_samples(np.array(self.plot_x_lim))
                _max = np.argmax(average.output_node.data[_samples[0]: _samples[-1], max_chan]) + _samples[0]
                _min = np.argmin(average.output_node.data[_samples[0]: _samples[-1], max_chan]) + _samples[0]
            else:
                _max = np.argmax(average.output_node.data[:, max_chan])
                _min = np.argmin(average.output_node.data[:, max_chan])
            times = np.sort(np.array(average.output_node.x[np.array([_max, _min])]))

            # plot time potential fields for average channel
            plotter = PlotTopographicMap(average,
                                         user_naming_rule=self.user_naming_rule + '_component_{:}'.format(_component),
                                         topographic_channels=np.array([cha_max_pow_label]),
                                         plot_x_lim=self.plot_x_lim,
                                         plot_y_lim=self.plot_y_lim,
                                         title='Component {:}'.format(_component),
                                         times=times)
            plotter.run()


class AutoRemoveBadChannels(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, thr_sd=5.0, amp_thr=50000.0, interval=0.001, **kwargs):
        """
        This function will try to detect bad channels by looking at the standard deviation across channels.
        It will remove any channels with and std largerd than thr_sd, which is computed across all channels.
        It also remove any channel which amplitude exceeds amp_thr.
        :param input_process: input_process: InputOutputProcess Class
        :param thr_sd: threshold standard deviation to remove channels. Channels with larger std will be removed
        :param amp_thr: threshold ampltitude. Channel exceeding this will be removed
        :param interval: time interval to subsample the data before estimating std (this is used to speed up the process
        in very long data files
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AutoRemoveBadChannels, self).__init__(input_process=input_process, **kwargs)
        self.thr_sd = thr_sd
        self.amp_thr = amp_thr
        self.interval = interval

    def transform_data(self):
        if self.input_node.data.ndim == 3:
            data = et_unfold(self.input_node.data)
        else:
            data = self.input_node.data
        _samples = np.arange(0, self.input_node.data.shape[0], int(self.interval * self.input_node.fs)).astype(np.int)
        sub_data = data[_samples, :]
        # compute dc component
        _dc_component = np.mean(np.abs(sub_data), axis=0)
        bad_channels = np.where(_dc_component > self.amp_thr)[0]
        _others_idx = np.array([idx for idx in np.arange(sub_data.shape[1]) if idx not in bad_channels], dtype=np.int)
        a_std = np.std(sub_data[:, _others_idx], axis=0)
        thr_ci = self.thr_sd * np.std(a_std) + np.mean(a_std)
        n_ch_idx = np.where(a_std > thr_ci)[0]
        bad_idx = _others_idx[n_ch_idx] if n_ch_idx.size else np.array([], dtype=np.int)
        bad_channels = np.concatenate((bad_channels, bad_idx))
        _bad_channels_index = np.unique(bad_channels)
        self.output_node.data = self.input_node.data.copy()
        self.output_node.delete_channel_by_idx(_bad_channels_index)


class RemoveBadChannels(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, bad_channels: [str] = None, **kwargs):
        """
        This class will remove any channel passed in bad_channel
        :param input_process: InputOutputProcess Class
        :param bad_channels: list of strings with the label of the channels to be removed
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RemoveBadChannels, self).__init__(input_process=input_process, **kwargs)
        if bad_channels is None:
            bad_channels = ['']
        self.bad_channels = bad_channels

    def transform_data(self):
        idx_bad_channels = [_i for _i, _item in enumerate(self.input_node.layout) if _item.label in self.bad_channels]
        _bad_channels_index = np.unique(idx_bad_channels)
        self.output_node.data = self.input_node.data.copy()
        if _bad_channels_index.size:
            self.output_node.delete_channel_by_idx(_bad_channels_index)


class AverageEpochs(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 weighted_average: bool = True,
                 n_tracked_points: int = 256,
                 block_size: int = 5,
                 roi_windows: np.array([TimeROI]) = None,
                 **kwargs):
        """
        This InputOutputProcess average epochs
        :param input_process: InputOutputProcess Class
        :param weighted_average: if True, average is computed using weighted average
        :param n_tracked_points: number of equally spaced points over time used to estimate residual noise
        :param block_size: number of trials that will be stack together to estimate the residual noise
        :param roi_windows: time windows used to perform some measures (snr, rms)
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AverageEpochs, self).__init__(input_process=input_process, **kwargs)
        self.weighted_average = weighted_average
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.roi_windows = roi_windows

    def transform_data(self):
        roi_samples = None
        if self.roi_windows is not None:
            roi_samples = np.array([_roi.get_samples_interval(fs=self.input_node.fs) for _roi in self.roi_windows])

        w_ave, w, rn, cum_rn, snr, cum_snr, s_var, w_fft, nk = \
            et_mean(epochs=self.input_node.data,
                    block_size=max(self.block_size, 5),
                    samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)),
                    roi_windows=roi_samples,
                    weighted=self.weighted_average
                    )
        self.output_node.data = w_ave
        self.output_node.rn = rn
        self.output_node.cum_rn = cum_rn
        self.output_node.snr = snr
        self.output_node.cum_snr = cum_snr
        self.output_node.s_var = s_var


class AverageEpochsFrequencyDomain(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 n_tracked_points:int = 256,
                 block_size: int = 5,
                 test_frequencies: np.array = None,
                 n_fft: int = None,
                 **kwargs):
        """
        This InputOutputProcess average epochs in the frequency domain
        :param input_process: InputOutputProcess Class
        :param n_tracked_points: number of equally spaced points over time used to estimate residual noise
        :param block_size: number of trials that will be stack together to estimate the residual noise
        :param test_frequencies: numpy array with frequencies that will be used to compute statistics (Hotelling test)
        :param n_fft: number of fft points
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AverageEpochsFrequencyDomain, self).__init__(input_process=input_process, **kwargs)
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.test_frequencies = test_frequencies
        self.n_fft = n_fft

    def transform_data(self):
        # average processed data across epochs including frequency average
        if self.n_fft is None:
            self.n_fft = self.input_node.data.shape[0]
        w_ave, rn, snr, w_fft, _ht_tests, s_var = \
            et_frequency_mean(epochs=self.input_node.data,
                              fs=self.input_node.fs,
                              n_fft=self.n_fft,
                              test_frequencies=self.test_frequencies,
                              block_size=max(self.block_size, 5),
                              samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)))

        self.output_node.data = w_fft
        self.output_node.x_units = u.Hz
        self.output_node.n_fft = self.n_fft
        self.output_node.domain = Domain.frequency
        self.output_node.rn = rn
        self.output_node.snr = snr
        self.output_node.s_var = s_var
        self.output_node.statistical_tests = self.get_ht_tests_as_pandas(_ht_tests)
        self.output_node.peak_frequency = self.get_frequency_peaks_as_pandas(_ht_tests)

    def get_ht_tests_as_pandas(self, hotelling_tests: np.array([HotellingTSquareFrequencyTest]) = None):
        f_peaks = []
        for _i, (_ht_ch, _ch) in enumerate(zip(hotelling_tests, self.input_node.layout)):
            for _s_ht in _ht_ch:
                _s_ht.channel = _ch.label
                f_peaks.append(_s_ht.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd

    def get_frequency_peaks_as_pandas(self, hotelling_tests: [np.array(HotellingTSquareFrequencyTest)] = None):
        f_peaks = []
        for _i, (_ht_ch, _ch) in enumerate(zip(hotelling_tests, self.input_node.layout)):
            for _s_ht in _ht_ch:
                _s_ht.channel = _ch.label
                _f_peak = EegPeak(channel=_ch.label,
                                  x=_s_ht.frequency_tested,
                                  idx=_s_ht.frequency_bin,
                                  rn=_s_ht.rn,
                                  snr=_s_ht.snr,
                                  signal_variance=self.output_node.s_var[0][_i],
                                  amp=_s_ht.spectral_magnitude,
                                  amp_snr=_s_ht.snr,
                                  significant=bool(_s_ht.p_value < 0.05),
                                  peak_label="{:10.1f}".format(_s_ht.frequency_tested),
                                  show_label=True,
                                  positive=True,
                                  domain=Domain.frequency,
                                  spectral_phase=_s_ht.spectral_phase)
                f_peaks.append(_f_peak.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd


class InterpolateData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 ini_end_points: np.array = np.array([[], []]),
                 save_plots: bool = True,
                 n_plots: int = 9,
                 fig_format: str = '.png',
                 fontsize: float = 8,
                 idx_channels_to_plot: np.array = np.array([0]),
                 user_naming_rule: str = 'interpolation',
                 **kwargs):
        """
        This class will interpolate data on each channel at a between two points passed in ini_end_points
        :param input_process: an InputOutput process
        :param ini_end_points: a Nx2 numpy array with the interpolation points
        :param save_plots: bool, if true, figures will be generated and saved showing a zoomed progression of the interp
        :param n_plots: int: number of plots (with different zoom scales) to be generated
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param fontsize: size of fonts in plot
        :param idx_channels_to_plot: np.array indicating the index of the channels to plot
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(InterpolateData, self).__init__(input_process=input_process, **kwargs)
        self.ini_end_points = ini_end_points
        self.save_plots = save_plots
        self.n_plots = n_plots
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.idx_channels_to_plot = idx_channels_to_plot
        self.user_naming_rule = user_naming_rule
        self.fontsize = fontsize

    def transform_data(self):
        if self.ini_end_points.size:
            print('Interpolating data')
            idx_delete = np.concatenate((np.where(self.ini_end_points[:, 0] >
                                                  self.input_node.data.shape[0] - 1)[0],
                                         np.where(self.ini_end_points[:, 1] >
                                                  self.input_node.data.shape[0] - 1)[0]))

            if np.any(idx_delete):
                self.ini_end_points = np.delete(self.ini_end_points, idx_delete, 0)
            self.output_node.data = self.input_node.data.copy()
            for _i in range(self.ini_end_points.shape[0]):
                _ini = self.ini_end_points[_i, 0]
                _end = self.ini_end_points[_i, 1]
                new_x = np.linspace(_ini, _end, num=_end - _ini + 1).astype(np.int)
                new_x_ar = np.tile(np.atleast_2d(new_x).T, (1, self.input_node.data.shape[1]))
                self.output_node.data[new_x, :] = ((new_x_ar - _ini) * (self.output_node.data[_end, :] -
                                                                        self.output_node.data[_ini, :]) /
                                                   (_end - _ini) + self.output_node.data[_ini, :])
            if self.save_plots:
                for _idx_channel in self.idx_channels_to_plot:
                    fig, ax = plt.subplots(1, 1)
                    ax.plot(self.input_node.x, self.input_node.data[:, _idx_channel])
                    ax.plot(self.input_node.x, self.output_node.data[:, _idx_channel])
                    ax.plot(self.input_node.x[self.ini_end_points[:, 0]],
                            self.output_node.data[self.ini_end_points[:, 0], _idx_channel],
                            'o', color='k', markersize=2)
                    ax.plot(self.input_node.x[self.ini_end_points[:, 1]],
                            self.output_node.data[self.ini_end_points[:, 1], _idx_channel],
                            'o', color='r', markersize=2)
                    m_point = self.ini_end_points.shape[0] // 2
                    figure_dir_path = self.input_node.paths.figures_current_dir
                    figure_basename = self.__class__.__name__ + '_' + self.user_naming_rule
                    for _i in range(self.n_plots):
                        _label = self.input_node.layout[_idx_channel].label
                        _fig_path = figure_dir_path + os.path.sep + _label + '_' + figure_basename + \
                                    '_{:}'.format(_i, self.fig_format)
                        _ini = max(0, m_point - 2 * 2 ** _i)
                        _end = min(self.ini_end_points.shape[0], m_point + 2 * 2 ** _i)
                        ax.set_xlim(self.input_node.x[self.ini_end_points[_ini, 0]],
                                    self.input_node.x[self.ini_end_points[_end, 0]])
                        ax.autoscale_view()
                        ax.set_xlabel('Time [{:}]'.format(self.input_node.x_units), fontsize=self.fontsize)
                        ax.set_ylabel('Amplitude [{:}]'.format(self.input_node.y_units), fontsize=self.fontsize)
                        ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
                        fig.tight_layout()
                        fig.savefig(_fig_path)
                        print('saving interpolated figures in {:}'.format(_fig_path))
                    fig.clf()


class PeriodicInterpolation(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 interpolation_rate=None,
                 interpolation_width=0.0,
                 interpolation_offset=0.0,
                 event_code=0.0, **kwargs):
        """
        This class will interpolate data on each channel at a given rate using as a reference time the position of
        trigger events
        :param input_process: InputOutputProcess Class
        :param interpolation_rate: rate (in Hz) to interpolate data points
        :param interpolation_width: the width (in sec) of the interpolation region
        :param interpolation_offset: an offset (in sec) in reference to trigger events
        :param event_code: the event code that will be use as reference to start interpolating
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PeriodicInterpolation, self).__init__(input_process=input_process, **kwargs)
        self.interpolation_rate = interpolation_rate
        self.interpolation_width = interpolation_width
        self.interpolation_offset = interpolation_offset
        self.event_code = event_code

    def transform_data(self):
        _events = self.input_node.events.get_events(self.event_code)
        _event_times = [_e.time_pos for _e in _events]

        _ini_points = np.array([])
        for _start, _end in zip(_event_times[:-1], _event_times[1::]):
            if self.interpolation_rate is not None:
                _ini_points = np.concatenate((_ini_points, np.arange(_start, _end, 1. / self.interpolation_rate)))
            else:
                _ini_points = np.append(_ini_points, _start)

        if self.interpolation_rate is not None:
            _ini_points = np.concatenate((_ini_points, np.arange(_event_times[-1],
                                                                 self.input_node.data.shape[0] /
                                                                 self.input_node.fs,
                                                                 1. / self.interpolation_rate)))
        _ini_points -= self.interpolation_offset
        _end_points = _ini_points + self.interpolation_width
        ini_end_points = (np.array([_ini_points, _end_points]).T * self.input_node.fs).astype(np.int)
        interp_data = InterpolateData(self.input_process, ini_end_points=ini_end_points)
        interp_data.run()
        self.output_node = interp_data.output_node


class InduceResponse(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, weighted_average=True, n_tracked_points=256, block_size=5,
                 roi_windows=None, **kwargs):
        super(InduceResponse, self).__init__(input_process=input_process, **kwargs)
        self.weighted_average = weighted_average
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.roi_windows = roi_windows

    def transform_data(self):
        trials_abs_w_ave, w, rn, cum_rn, snr, cum_snr, s_var, w_fft, nk = \
            et_mean(epochs=np.abs(self.input_node.data),
                    block_size=max(self.block_size, 5),
                    samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)),
                    roi_windows=self.roi_windows,
                    weighted=self.weighted_average
                    )
        w_ave, w, rn, cum_rn, snr, cum_snr, s_var, w_fft, nk = \
            et_mean(epochs=self.input_node.data,
                    block_size=max(self.block_size, 5),
                    samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)),
                    roi_windows=self.roi_windows,
                    weighted=self.weighted_average
                    )
        self.output_node.data = trials_abs_w_ave - np.abs(w_ave)
        self.output_node.rn = rn
        self.output_node.cum_rn = cum_rn
        self.output_node.snr = snr
        self.output_node.cum_snr = cum_snr
        self.output_node.s_var = s_var


class AverageTimeFrequencyResponse(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 time_window=0.3,
                 sample_interval=0.004,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 spec_thresh=4,
                 average_mode='magnitude',
                 **kwargs):
        super(AverageTimeFrequencyResponse, self).__init__(input_process=input_process, **kwargs)
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.frequency = None
        self.time = None
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.spec_thresh = spec_thresh
        self.average_mode = average_mode

    def transform_data(self):
        power, time, freqs = et_average_time_frequency_transformation(epochs=self.input_node.data,
                                                                      fs=self.input_node.fs,
                                                                      time_window=self.time_window,
                                                                      sample_interval=self.sample_interval,
                                                                      average_mode=self.average_mode
                                                                      )
        self.output_node.data = power
        self.output_node.x = time
        self.output_node.y = freqs
        self.output_node.domain = Domain.time_frequency


class PlotTimeFrequencyData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 spec_thresh=4,
                 **kwargs):
        super(PlotTimeFrequencyData, self).__init__(input_process=input_process, **kwargs)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.spec_thresh = spec_thresh

    def transform_data(self):
        assert self.input_node.domain == Domain.time_frequency, 'input should be a time-frequency transformed data'

        eegpt.plot_eeg_time_frequency_power(ave_data=self.input_node,
                                            time=self.input_node.x,
                                            frequency=self.input_node.y,
                                            eeg_topographic_map_channels=self.topographic_channels,
                                            figure_dir_path=self.input_node.paths.figures_current_dir,
                                            figure_basename=self.input_process.__class__.__name__ + '_' +
                                                            self.user_naming_rule,
                                            time_unit='s',
                                            amplitude_unit='uV',
                                            times=self.times,
                                            title=self.title,
                                            x_lim=self.plot_x_lim,
                                            y_lim=self.plot_y_lim,
                                            fig_format=self.fig_format,
                                            fontsize=self.fontsize,
                                            spec_thresh=self.spec_thresh
                                            )


class PlotTimeWaveforms(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 idx_ch_to_plot: np.array = None,
                 title: str = '',
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 fig_format: str = '.png',
                 fontsize: float = 12,
                 user_naming_rule: str = '',
                 **kwargs):
        """
        This InputOutputProcess plots all channel waveforms in a single plot and saves them.
        :param input_process: an InputOutput process
        :param idx_ch_to_plot: numpy array with index of channels to plot
        :param title: title of the plot
        :param plot_x_lim: x limits of the plot
        :param plot_y_lim: y limits of the plot
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param fontsize: size of fonts in plot
        :param idx_channels_to_plot: np.array indicating the index of the channels to plot
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PlotTimeWaveforms, self).__init__(input_process=input_process, **kwargs)
        self.idx_ch_to_plot = idx_ch_to_plot
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule

    def transform_data(self):
        assert self.input_node.domain == Domain.time, 'input should be a time-domain data'
        figure_dir_path = self.input_node.paths.figures_current_dir
        figure_basename = self.input_process.__class__.__name__ + '_' + self.user_naming_rule
        eegpt.plot_time_single_channels(ave_data=self.input_node,
                                        idx_ch_to_plot=self.idx_ch_to_plot,
                                        figure_dir_path=figure_dir_path,
                                        figure_basename=figure_basename,
                                        title=self.title,
                                        x_lim=self.plot_x_lim,
                                        y_lim=self.plot_y_lim,
                                        fig_format=self.fig_format,
                                        fontsize=self.fontsize
                                        )


class AverageFrequencyPower(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 **kwargs):
        super(AverageFrequencyPower, self).__init__(input_process=input_process, **kwargs)
        self.frequency = None
        self.time = None
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times

    def transform_data(self):
        power, freqs = et_average_frequency_transformation(epochs=self.input_node.data,
                                                           fs=self.input_node.fs,
                                                           ave_mode='magnitude'
                                                           )
        self.output_node.data = power


class PeakDetectionTimeDomain(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, time_peak_windows=np.array([TimePeakWindow]),
                 peak_to_peak_measures=np.array([PeakToPeakMeasure]), **kwargs):
        super(PeakDetectionTimeDomain, self).__init__(input_process=input_process, **kwargs)
        self.time_peak_windows = time_peak_windows
        self.peak_to_peak_measures = peak_to_peak_measures

    def transform_data(self):
        peak_containers, amplitudes = detect_peaks_and_amplitudes(
            data_node=self.input_node,
            time_peak_windows=self.time_peak_windows,
            eeg_peak_to_peak_measures=self.peak_to_peak_measures)

        _data_pd = pd.concat([_peaks.to_pandas() for _peaks in peak_containers])
        _amps_pd = pd.concat([_amp.to_pandas() for _amp in amplitudes])
        self.output_node.data = self.input_node.data
        self.output_node.peak_times = _data_pd
        self.output_node.peak_to_peak_amplitudes = _amps_pd


class PlotTopographicMap(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels: np.array([str]) = np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 **kwargs
                 ):
        super(PlotTopographicMap, self).__init__(input_process=input_process, **kwargs)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times

    def transform_data(self):
        _figure_base_name = self.input_process.__class__.__name__ + self.user_naming_rule
        eegpt.plot_eeg_topographic_map(ave_data=self.input_node,
                                       eeg_topographic_map_channels=self.topographic_channels,
                                       figure_dir_path=self.input_node.paths.figures_current_dir,
                                       figure_basename=_figure_base_name,
                                       times=self.times,
                                       domain=self.input_node.domain,
                                       title=self.title,
                                       x_lim=self.plot_x_lim,
                                       y_lim=self.plot_y_lim,
                                       fig_format=self.fig_format,
                                       fontsize=self.fontsize)


class AverageSpectrogram(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 time_window=2.0,
                 sample_interval=0.004,
                 spec_thresh=4
                 ):
        super(PlotSpectrogram, self).__init__(input_process=input_process)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.spec_thresh = spec_thresh

    def transform_data(self):
        power, freqs = et_average_frequency_transformation(epochs=self.input_node.data,
                                                           fs=self.input_node.fs,
                                                           ave_mode='magnitude'
                                                           )
        self.output_node.data = power


class PlotSpectrogram(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 time_window=2.0,
                 sample_interval=0.004,
                 spec_thresh=4
                 ):
        super(PlotSpectrogram, self).__init__(input_process=input_process)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.spec_thresh = spec_thresh

    def transform_data(self):
        eegpt.plot_eeg_time_frequency_transformation(ave_data=self.input_node,
                                                     eeg_topographic_map_channels=self.topographic_channels,
                                                     figure_dir_path=self.input_node.paths.figure_basename_path,
                                                     figure_basename=self.input_process.__class__.__name__ + '_' +
                                                                     self.user_naming_rule,
                                                     time_unit='s',
                                                     amplitude_unit='uV',
                                                     times=self.times,
                                                     domain=self.input_node.domain,
                                                     title=self.title,
                                                     x_lim=self.plot_x_lim,
                                                     y_lim=self.plot_y_lim,
                                                     fig_format=self.fig_format,
                                                     fontsize=self.fontsize,
                                                     time_window=self.time_window,
                                                     sample_interval=self.sample_interval,
                                                     spec_thresh=self.spec_thresh
                                                     )


class DeEpoch(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, de_mean=True, **kwargs):
        """
        This function will convert a n*m*p matrix into an (n*p) * m matrix
        :param input_process: InputOutputProcess Class
        :param de_mean: whether data should be demeaned before convering
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(DeEpoch, self).__init__(input_process=input_process, **kwargs)
        self.de_mean = de_mean

    def transform_data(self):
        _data = np.array(self.input_node.data.data)
        # if self.de_mean:
        #     _data -= np.mean(_data, axis=0)
        for _i in range(1, _data.shape[2]):
            _data[:, :, _i] = _data[:, :, _i] - (_data[0, :, _i] - _data[-1, :, _i - 1])

        self.output_node.data = np.reshape(np.transpose(_data, [0, 2, 1]), [-1, _data.shape[1]], order='F')
        events = []
        for _i in range(_data.shape[2]):
            events.append(SingleEvent(code=1,
                                      time_pos=_i * _data.shape[0] / self.input_node.fs,
                                      dur=0))
        events = Events(events=np.array(events))
        self.output_node.events = events


class HotellingT2Test(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 block_time=0.040,
                 detrend_data=False,
                 roi_windows: np.array(TimeROI) = None,
                 weighted_average=True,
                 **kwargs) -> InputOutputProcess:
        """
        This class computes the statistical significance of a region of interest using a Hotelling test.
        The time region where a response is expected is tested to assess whether the linear combination of data points
        within this region is significantly different of a random linear combination of points.

        :param input_process: input_process: InputOutputProcess Class
        :param block_time: the length (in secs) of each average window within a region of interest (roi). A ROI will be
        divided in several points of duration block_time. The average of each block_time will represent a sample passed
        for the statistical test
        :param detrend_data: whether to remove or not any linear component on each trial before performing the test
        :param roi_windows: a region of windows containing the response of interest
        :param weighted_average: if True, estimations are based on a weighted averaging rather than standard averaging
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(HotellingT2Test, self).__init__(input_process=input_process, **kwargs)
        self.block_time = block_time
        self.detrend_data = detrend_data
        self.roi_windows = roi_windows
        self.weighted_average = weighted_average

    def transform_data(self):
        block_size = int(self.block_time * self.input_node.fs)
        # compute weighted average to extract weights
        w_ave, w, rn, cum_rn, snr, cum_snr, s_var, w_fft, nk = \
            et_mean(epochs=self.input_node.data,
                    block_size=5,
                    samples_distance=int(max(self.input_node.data.shape[0] // 256, 10)),
                    weighted=self.weighted_average
                    )

        for _roi_idx, _roi in enumerate(self.roi_windows):
            _samples = _roi.get_samples_interval(fs=self.input_node.fs)
            data = self.input_node.data.copy()
            # remove linear trend
            _ini = _samples[0]
            _end = _samples[1]
            if self.detrend_data:
                x = np.expand_dims(np.arange(0, data[_ini:_end, :, :].shape[0]), axis=1)
                for _idx in np.arange(data.shape[2]):
                    _ini_dt = np.maximum(0, _ini - int(self.fs * 0.1))
                    _end_dt = np.minimum(data.shape[0], _end + int(self.fs * 0.1))
                    _subset = data[_ini_dt:_end_dt, :, _idx].copy()
                    x_dt = np.expand_dims(np.arange(0, data[_ini_dt:_end_dt, :, :].shape[0]), axis=1)
                    regression = LinearRegression()
                    regression.fit(x_dt, _subset)
                    data[_ini:_end, :, _idx] -= regression.predict(x)
            # remove mean
            data[_ini:_end, :, :] -= np.mean(data[_ini:_end, :, :], axis=0)
            block_size = max(0, min(block_size, _samples[1] - _samples[0]))
            n_blocks = np.floor((_samples[1] - _samples[0]) / block_size).astype(np.int)
            samples = np.array([np.mean(data[_ini + _i * block_size: _ini + (_i + 1) * block_size, :, :], 0)
                                for _i in range(n_blocks)])
            h_tests = hotelling_t_square_test(samples, weights=w, channels=self.input_node.layout, **_roi.__dict__)

        self.output_node.statistical_tests = self.get_ht_tests_as_pandas(h_tests)

    @staticmethod
    def get_ht_tests_as_pandas(hotelling_tests: np.array([dict]) = None):
        _data_pd = pd.DataFrame(hotelling_tests)
        return _data_pd


class FTest(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 delta_frequency=20.,
                 test_frequencies: np.array = None,
                 alpha: float = 0.05,
                 n_fft: int = None,
                 **kwargs) -> InputOutputProcess:
        """
        This class computes a FTest in the frequency domain of a signal.
        :param input_process: input_process: InputOutputProcess Class
        :param delta_frequency: the length in Hz around each target frequency to compute statistics
        :param test_frequencies: a numpy array with the frequencies that will be tested
        :param alpha: level to assess significance of the test
        :param n_fft: number of points to perform fft transformation
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(FTest, self).__init__(input_process=input_process, **kwargs)
        self.delta_frequency = delta_frequency
        self.test_frequencies = test_frequencies
        self.fft_frequencies: np.array = None
        self.alpha = alpha
        self.n_fft = n_fft

    def transform_data(self):
        assert self.input_node.domain == Domain.time
        if self.n_fft is None:
            self.n_fft = self.input_node.data.shape[0]
        fft = pyfftw.builders.rfft(self.input_node.data, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
                                   threads=multiprocessing.cpu_count(), n=self.n_fft)
        w_fft = fft()
        w_fft /= self.input_node.data.shape[0] / 2
        self.fft_frequencies = np.arange(w_fft.shape[0]) * self.input_node.fs / self.n_fft
        results = [[]] * self.input_node.data.shape[1]
        for _freq in self.test_frequencies:
            _target_bin = self.freq_to_samples(_freq)
            _ini_bin = np.maximum(self.freq_to_samples(_freq - self.delta_frequency), 0)
            _end_bin = np.minimum(self.freq_to_samples(_freq + self.delta_frequency), w_fft.shape[0] - 1)
            _neighbours_idx = np.concatenate((
                np.arange(_ini_bin, _target_bin),
                np.arange(_target_bin + 1, _end_bin + 1)))
            _n = _neighbours_idx.size
            _f_critic = f.ppf(1 - self.alpha, 2, 2 * _n)
            _rns = np.sqrt(np.sum(np.abs(w_fft[_neighbours_idx, :]) ** 2, axis=0))
            _f_tests = _n * np.abs(w_fft[_target_bin, :]) ** 2 / _rns ** 2.0

            for _ch, _f in enumerate(_f_tests):
                _d1 = 2
                _d2 = 2 * _n
                _p_values = 1 - f.cdf(_f, _d1, _d2)
                _snr = np.maximum(_f - 1, 0.0)
                _test_out = FrequencyFTest(frequency_tested=_freq,
                                           frequency_bin=_target_bin,
                                           df_1=_d1,
                                           df_2=_d2,
                                           f=_f,
                                           p_value=_p_values,
                                           spectral_magnitude=np.abs(w_fft[_target_bin, _ch]),
                                           spectral_phase=np.angle(w_fft[_target_bin, _ch]),
                                           rn=_rns[_ch],
                                           snr=_snr,
                                           snr_db=10 * np.log10(_snr) if _snr > 0.0 else -np.Inf,
                                           snr_critic_db=10 * np.log10(_f_critic - 1),
                                           snr_critic=_f_critic - 1)
                results[_ch] = np.append(results[_ch], [_test_out])

        # self.output_node.ht_tests = self.get_ht_tests_as_pandas(h_tests)
        self.output_node.data = w_fft
        self.output_node.n_fft = self.n_fft
        self.output_node.domain = Domain.frequency
        self.output_node.statistical_tests = self.get_f_tests_as_pandas(results)
        self.output_node.peak_frequency = self.get_frequency_peaks_as_pandas(f_tests=results)

    def get_frequency_peaks_as_pandas(self, f_tests: [np.array(FrequencyFTest)] = None):
        f_peaks = []
        for _i, (_f_ch, _ch) in enumerate(zip(f_tests, self.input_node.layout)):
            for _s_ft in _f_ch:
                _s_ft.label = _ch.label
                _f_peak = EegPeak(channel=_ch.label,
                                  x=_s_ft.frequency_tested,
                                  idx=_s_ft.frequency_bin,
                                  rn=_s_ft.rn,
                                  snr=_s_ft.snr,
                                  signal_variance=self.output_node.s_var[0][_i],
                                  amp=_s_ft.spectral_magnitude,
                                  amp_snr=_s_ft.snr,
                                  significant=bool(_s_ft.p_value < 0.05),
                                  peak_label="{:10.1f}".format(_s_ft.frequency_tested),
                                  show_label=True,
                                  positive=True,
                                  domain=Domain.frequency,
                                  spectral_phase=_s_ft.spectral_phase)
                f_peaks.append(_f_peak.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd

    def freq_to_samples(self, value: np.array) -> np.array:
        if isinstance(value, float):
            value = [value]
        out = np.array([])
        for _v in value:
            out = np.append(out, np.argmin(np.abs(self.fft_frequencies - np.array(_v)))).astype(np.int)
        return np.squeeze(out)

    def get_f_tests_as_pandas(self, f_tests: [np.array(HotellingTSquareFrequencyTest)] = None):
        f_peaks = []
        for _i, (_ht_ch, _ch) in enumerate(zip(f_tests, self.input_node.layout)):
            for _s_ht in _ht_ch:
                _s_ht.channel = _ch.label
                f_peaks.append(_s_ht.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd


class AppendGFPChannel(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 channel_label: str = 'GFP',
                 **kwargs):
        """
        This InputOutputProcess will append the global field power (standard deviation across all channels)
        :param input_process: InputOutputProcess Class
        :param channel_label: name of new channel in layout
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AppendGFPChannel, self).__init__(input_process=input_process, **kwargs)
        self.channel_label = channel_label

    def transform_data(self):
        data = self.input_node.data
        new_channel_data = np.std(data, axis=1, keepdims=True)
        self.output_node.data = data
        self.output_node.append_new_channel(new_data=new_channel_data, layout_label=self.channel_label)


class RegressOutArtifact(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 event_code: float = None,
                 alternating_polarity: bool = False,
                 stimulus_waveform: np.array = None,
                 method: str = 'regression',
                 **kwargs):
        super(RegressOutArtifact, self).__init__(input_process=input_process, **kwargs)
        self.event_code = event_code
        self.alternating_polarity = alternating_polarity
        self.stimulus_waveform = stimulus_waveform
        self.method = method

    def transform_data(self):
        data = self.input_node.data.copy()
        # convolve source with dirac train to generate entire signal
        _events_index = self.input_node.events.get_events_index(code=self.event_code, fs=self.input_node.fs)
        events = np.zeros(data.shape[0])
        events[_events_index] = 1
        if self.alternating_polarity:
            events[_events_index[1: -1: 2]] = -1
        #  convolve source with triggers
        source = filt_data(input_data=self.stimulus_waveform, b=events.flatten(), mode='full', onset_padding=False)
        source = source[0:data.shape[0]]
        if self.method == 'regression':
            for _idx in np.arange(data.shape[1]):
                regression = LinearRegression()
                regression.fit(source, data[:, _idx])
                data[:, _idx] -= regression.predict(source)
        # if self.method == 'ir':
        #     _fft = pyfftw.builders.fft(data, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
        #                                threads=multiprocessing.cpu_count())
        #     fxy = _fft()
        #     _fft = pyfftw.builders.fft(source, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
        #                                threads=multiprocessing.cpu_count())
        #     fxx = _fft()
        #     fh = fxy/fxx
        #     _ifft = pyfftw.builders.ifft(fh, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
        #                                  threads=multiprocessing.cpu_count())
        #     h = _ifft()
        if self.method == 'xcorr':
            transmission_index = data.T.dot(source) / np.expand_dims(
                np.sum(np.square(source)), axis=0)
            data -= np.repeat(source, data.shape[1], axis=1) * transmission_index.T

        self.output_node.data = data
