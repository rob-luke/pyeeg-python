import numpy as np
from scipy.stats import f
from pyeeg.processing.statistics.definitions import HotellingTSquareTest
from pyeeg.definitions.channel_definitions import ChannelItem


def hotelling_t_square_test(samples=np.array([]),
                            weights: np.array = None,
                            channels: np.array([ChannelItem]) = None, **kwargs):
    _test_data = []
    if not samples.size:
        return None
    if weights is None:
        weights = np.ones(samples.shape[1::])

    # standard average
    # mean_data = np.mean(samples, 2)
    # dof = np.ones(samples.shape[1]) * samples.shape[2]
    mean_data = np.sum(weights * samples, axis=2) / np.sum(weights, axis=1)
    dof = np.sum(weights, axis=1) ** 2.0 / np.sum(weights ** 2.0, axis=1)
    with np.errstate(divide='ignore', invalid='ignore'):
        for _ch in range(samples.shape[1]):
            data_ch = samples[:, _ch, :]
            mean_ch = mean_data[:, _ch]
            _cov_mat = np.cov(data_ch, aweights=weights[_ch, :])
            # if np.linalg.cond(_cov_mat) < 1 / sys.float_info.epsilon:
            try:
                _t_square = dof[_ch] * mean_ch.dot(np.linalg.inv(_cov_mat)).dot(mean_ch.T)
            except:
                try:
                    _t_square = dof[_ch] * mean_ch.dot(np.linalg.inv(_cov_mat +
                                                                             np.eye(_cov_mat.shape[1]) * 10.e-10)).dot(mean_ch.T)
                except:
                    _t_square = np.inf
                    print('could not found the inverse covariance matrix')

            n_p = data_ch.shape[0]
            n_n = dof[_ch]
            _f = (n_n - n_p) / (n_p * (n_n - 1.0)) * _t_square
            _d1 = n_p
            _d2 = n_n - n_p

            c = f.ppf(0.5, _d1, _d2)
            if _f > c:
                p = f.cdf(1.0 / _f, _d2, _d1)
            else:
                p = 1 - f.cdf(_f, _d1, _d2)
            _f_95 = f.ppf(0.95, _d1, _d2)
            # compute residual noise from circular variance
            _rn = np.sqrt(np.sum((data_ch - np.expand_dims(mean_ch, 1)) ** 2.0)) / (data_ch.shape[1] - 1)
            _snr = np.maximum(_f - 1, 0.0)
            if channels is not None:
                _channel = channels[_ch].label
            else:
                _channel = str(_ch)

            # _L = ((2 * (self.split_sweep_count[i] - 1) * D * F95)./(self.split_sweep_count(i) * (self.split_sweep_count(i) - 2))) ** 2

            test = HotellingTSquareTest(t_square=_t_square,
                                        df_1=_d1,
                                        df_2=_d2,
                                        f=_f,
                                        p_value=p,
                                        # mean_amplitude=mean_ch,
                                        rn=_rn,
                                        snr=_snr,
                                        snr_db=10 * np.log10(_snr) if _snr > 0.0 else -np.Inf,
                                        snr_critic_db=10 * np.log10(_f_95 - 1),
                                        snr_critic=_f_95 - 1,
                                        f_critic=_f_95,
                                        channel=_channel)
            _test_data.append(dict(test.__dict__, **kwargs))
    return _test_data
