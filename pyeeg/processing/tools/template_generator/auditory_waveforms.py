import numpy as np
from scipy import interpolate
from scipy.signal import windows


def aep(fs=16384.0):
    """
    This function generates a typical auditory cortex waveform (as shown in  Human Auditory Evoked Potentials
    Terence W. Picton
    :param fs: sampling rate of the generated waveform (samples per seconds)
    :return: waveform and time vector
    """
    time = np.arange(0, 1.0, 1/fs)
    # Terrace Picton peaks latencies (in ms)
    _peak_times = np.array([0.0,
                            0.75,
                            1.5, 2.0,  # I
                            2.5, 3.0,  # II
                            3.5, 4.0,  # III
                            4.5, 5.0,  # IV
                            6.0, 7.0,  # V
                            7.5, 9.0,  # VI-N0
                            13.0, 18.0,  # P0-Na
                            30.0, 40.0,  # Pa-Nb
                            50., 100,  # P1-N1
                            180.0, 300.,   # P2-N2
                            600.0,
                            1000.0
                            ])*1e-3

    # Amplitudes in uV
    _peak_amplitudes = np.array([0.0,
                                 0.0,
                                 0.15, 0.0,  # I
                                 0.1, -0.02,  # II
                                 0.2, 0.03,  # III
                                 0.4, 0.35,  # IV
                                 0.5, 0.1,  # V
                                 0.15, -0.4,  # VI-N0
                                 0.1, -0.7,  # P0-Na
                                 0.8, -0.7,  # Pa-Nb
                                 0.9, -2.0,  # P1-N1
                                 2.5, -1.0,  # P2-N2
                                 -0.2,
                                 0
                                 ])
    f = interpolate.interp1d(_peak_times, _peak_amplitudes, kind='linear')
    _x = np.logspace(np.log10(time[1]), np.log10(time[-1]), num=75, endpoint=False)
    _y = f(_x)
    f2 = interpolate.interp1d(_x, _y, kind='quadratic', fill_value="extrapolate")
    ynew = f2(time).reshape(-1, 1)
    # plt.plot(_x, _y)
    # plt.plot(_peak_times, _peak_amplitudes, 'o')
    # plt.plot(time, ynew)
    # plt.show()
    return ynew, time


def eog(fs=16384.0):
    """
    This function generates a typical eye blink waveform with an amplitude of 300 uV
    :param fs: sampling rate of the generated waveform (samples per seconds)
    :return: waveform and time vector
    """
    time = np.arange(0, 3.0, 1/fs)
    event_time = np.arange(0, 2.0, 1/fs)
    event = windows.flattop(event_time.size) + windows.general_gaussian(event_time.size, p=1.5, sig=5000)
    event[0:event_time.size//2] += windows.general_gaussian(event_time.size//2, p=1.5, sig=3000)
    y_new = np.zeros(time.shape)
    y_new[0:event_time.size] = 300 * event / 2
    return y_new.reshape(-1, 1), time
