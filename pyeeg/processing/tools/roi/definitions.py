import numpy as np


class TimeROI(object):
    """
    Object used to define the timepoints in a region of interest (ROI), for time-window based analysis. The required
    parameters  are the beginning (initial) and ending timepoints in seconds. To distinguish different ROI windows,
    each objects can  be linked to a measure (string), a label (string), a condition (string), an (EEG) channel (str),
    or a value (float). This class will return a  numpy array. The TimeROI object is used by classes like AverageEpochs
    or HotellingT2Test.
    Example to define a ROI window for SNR estimation starting at 150 ms and ending at 350 ms:
    roi_windows = np.array([TimeROI(ini_time=150.0e-3, end_time=350.0e-3, measure="snr", label="itd_snr")])
    """
    def __init__(self,
                 ini_time: float = None,
                 end_time: float = None,
                 measure: str = None,
                 label: str = None,
                 condition: str = None,
                 value: float = None
                 ):
        """

        :param ini_time: initial time of the window (in seconds)
        :param end_time: end time of the window (in seconds)
        :param measure: str measure to be performed e.g. "snr", "rms" (this key value can be used to trigger a given
        action within a function requiring a ROI window
        :param label: str defining a desired label for this ROI
        :param condition: str defining a desired condition
        :param value: output value to store the output  of a given measure
        """
        self.ini_time = ini_time
        self.end_time = end_time
        self.measure = measure
        self.label = label
        self.condition = condition
        self.value = value

    def get_samples_interval(self, fs: float):
        out = np.array([self.ini_time, self.end_time]) * fs
        return out.astype(np.int)
