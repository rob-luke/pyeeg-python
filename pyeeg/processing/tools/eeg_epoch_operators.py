import numpy as np

__author__ = 'jaime undurraga'


def et_covariance(x=np.array([])):
    c = np.zeros((x.shape[1], x.shape[1]))
    for i in np.arange(x.shape[2]):
        c += x[:, :, i].T.dot(x[:, :, i])
    return c


def et_unfold(x, ortogonal=False):
    # unfold to improve machine performance during multiplications
    if ortogonal:
        return np.reshape(np.transpose(x, [0, 2, 1]), [-1, x.shape[1]], order='F')
    else:
        return np.reshape(np.transpose(x, [0, 2, 1]), [-1, x.shape[1]])


def et_fold(x, epoch_size):
    #  fold back to normal
    return np.transpose(np.reshape(x, [epoch_size, -1, x.shape[1]]), [0, 2, 1])


def et_mmat(x, m):
    return et_fold(et_unfold(x).dot(m), x.shape[0])


def et_x_covariance(x, y):
    c = np.zeros((x.shape[1], y.shape[1]))
    for i in np.arange(x.shape[2]):
        c += x[:, :, i].T.dot(y[:, :, i])
    return c


def et_x_join_covariance(x, y):
    # compute the covariance combining all epochs
    return et_unfold(x).T.dot(et_unfold(y))


def et_find_freq_bin(freq_vector: np.array = np.array([]),
                     freq_to_find: np.array = np.array([])):
    f_bins = []
    for f in freq_to_find:
        f_pos = np.argmin(np.abs(freq_vector - f))
        f_bins.append(f_pos)
    return f_bins


def et_w_covariance(epochs: np.array = np.array([])):
    c = np.zeros((epochs.shape[1], epochs.shape[1]))
    # w_mean, w = et_weighted_mean(epochs)
    # for i in np.arange(x.shape[2]):
    #     c = c + x[:, :, i].T.dot(x[:, :, i])
    # return cfol