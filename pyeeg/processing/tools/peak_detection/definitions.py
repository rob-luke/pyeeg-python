from pyeeg.definitions.channel_definitions import Domain
import numpy as np
import pandas as pd


class EegPeak(object):
    def __init__(self, channel: str = None, x: float = None, idx: int = None, rn: float = None, snr: float = None,
                 signal_variance: float = None, amp: float = None, amp_snr: float = None, curvature: float = None,
                 significant: bool = False,  peak_label: str = None, show_label: bool = True,
                 positive: bool = True,  domain: Domain = Domain.time, spectral_phase: float = None):
        self.channel = channel
        self.x = x
        self.idx = idx
        self.rn = rn
        self.snr = snr
        self.signal_variance = signal_variance
        self.amp = amp
        self.amp_snr = amp_snr
        self.curvature = curvature
        self.significant = significant
        self.peak_label = peak_label
        self.show_label = show_label
        self.positive = positive
        self.domain = domain
        self.spectral_phase = spectral_phase


class TimePeakWindow(object):
    def __init__(self, **kwargs):
        self.ini_ref = kwargs.get('ini_ref', None)
        self.end_ref = kwargs.get('end_ref', None)
        self.require_ini_ref = kwargs.get('require_ini_ref', False)
        self.require_end_ref = kwargs.get('require_end_ref', False)
        self.global_ref = kwargs.get('global_ref', None)
        self.require_global_ref = kwargs.get('require_ini_ref', False)
        self.ini_global_ref = kwargs.get('ini_global_ref', None)
        self.end_global_ref = kwargs.get('end_global_ref', None)
        self.minimum_scanning_window = kwargs.get('minimum_scanning_window', 1.0e-3)
        self.positive_peak = kwargs.get('positive_peak', True)
        self.use_wighted_curvature = kwargs.get('use_wighted_curvature', True)
        self.show_label = kwargs.get('show_label', True)
        self.show_window = kwargs.get('show_window', True)
        self.force_search = kwargs.get('force_search', True)
        self.channel = kwargs.get('channel', None)
        self.target_channel = kwargs.get('target_channel', None)
        self._label = kwargs.get('label', None)
        self._ini_time = kwargs.get('ini_time', None)
        self._end_time = kwargs.get('end_time', None)

    def get_ini_time(self):
        return self._ini_time

    def set_ini_time(self, value):
        self._ini_time = value

    ini_time = property(get_ini_time, set_ini_time)

    def get_end_time(self):
        _end_time = self._end_time
        if self._end_time is None:
            _end_time = self._ini_time
        return _end_time

    def set_end_time(self, value):
        self._end_time = value
    end_time = property(get_end_time, set_end_time)

    def set_label(self, value):
        self._label = value

    def get_label(self):
        _label = self._label
        if self._label is None:
            _label = '{:.2e}'.format(self.ini_time)
        return _label
    label = property(get_label, set_label)


class PeaksContainer:
    def __init__(self, channel_label: str = None):
        self.channel_label = channel_label
        self._peaks = []

    def get_peaks(self):
        return self._peaks

    def append(self, value: EegPeak = None):
        self._peaks.append(value)

    peaks = property(get_peaks)

    def to_pandas(self):
        _data_pd = pd.DataFrame([_peak.__dict__ for _peak in self._peaks])
        return _data_pd


class PeakToPeakMeasure(object):
    def __init__(self, ini_peak='', end_peak=''):
        self.ini_peak = ini_peak
        self.end_peak = end_peak


class PeakToPeakQuantity(object):
    def __init__(self, **kwargs):
        self.channel = kwargs.get('channel', None)
        self.ini_time = kwargs.get('ini_time', None)
        self.max_time = kwargs.get('max_time', None)
        self.min_time = kwargs.get('min_time', None)
        self.idx_ini = kwargs.get('idx_ini', None)
        self.idx_end = kwargs.get('idx_end', None)
        self.amp = kwargs.get('amp', None)
        self.rn = kwargs.get('rn', None)
        self.snr = kwargs.get('snr', None)
        self.amp_snr = kwargs.get('amp_snr', None)
        self.amp_label = kwargs.get('amp_label', None)


class PeakToPeakAmplitudeContainer:
    def __init__(self, channel_label: str = None):
        self.channel_label = channel_label
        self._peaks = []

    def get_peaks(self):
        return self._peaks

    def append(self, value: PeakToPeakQuantity = None):
        self._peaks.append(value)

    peaks = property(get_peaks)

    def to_pandas(self):
        _data_pd = pd.DataFrame([_peak.__dict__ for _peak in self._peaks])
        return _data_pd
