import numpy as np
from pyeeg.processing.tools import weightedAverage as JAve, eeg_epoch_operators as eo
from sklearn import linear_model
import matplotlib.pyplot as plt
import pyeeg.processing.tools.filters.spatial_filtering as sf
import pyfftw
import multiprocessing
from scipy import signal
import pycwt as wavelet
import pywt as pw
__author__ = 'jaime undurraga'


def et_get_epoch_weights_and_rn_weighted_average(epochs: np.array = np.array([]),
                                                 block_size: int = 5,
                                                 samples_distance: int = 10):
    # compute weights for averaging an residual noise estimation
    block_size = np.minimum(epochs.shape[2], block_size)
    blocks = np.arange(0, epochs.shape[2] + 1, block_size).astype(np.int)
    _epochs = epochs[np.arange(0, epochs.shape[0], samples_distance), :, :]
    blocks[-1] = np.maximum(blocks[-1], _epochs.shape[2])
    unfolded_epochs = eo.et_unfold(np.transpose(_epochs, [0, 2, 1]), ortogonal=True)
    var = np.var(unfolded_epochs[:, blocks[0]: blocks[1]], axis=1)
    # var2 = np.expand_dims(np.mean(np.var(_epochs[:, :, blocks[0]: blocks[1]], axis=2), axis=0), axis=1)
    for i in range(1, len(blocks) - 1):
        var = np.vstack((var, np.var(unfolded_epochs[:, blocks[i]: blocks[i+1]], axis=1)))

    wb = 1.0 / np.mean(eo.et_fold(var.T, _epochs.shape[1]), axis=2)
    nk = np.diff(blocks)
    w = np.ones((_epochs.shape[1], nk[0])) * np.expand_dims(wb[:, 0], 1)
    for i, n in enumerate(nk[1:]):
        w = np.concatenate((w, np.ones(n) * np.expand_dims(wb[:, i + 1], 1)), axis=1)
    rn = np.sqrt(1.0 / np.sum(nk * wb, axis=1))
    cumulative_rn = None
    for _i in range(wb.shape[1]):
        if _i == 0:
            cumulative_rn = np.sqrt(1.0 / np.sum(nk[0:_i + 1] * wb[:, 0:_i + 1], axis=1))
        else:
            cumulative_rn = np.vstack((cumulative_rn, np.sqrt(1.0 / np.sum(nk[0:_i+1] * wb[:, 0:_i + 1], axis=1))))
    return w, rn, cumulative_rn, nk


def et_get_epoch_weights_and_rn_standard_average(epochs: np.array = np.array([]),
                                                 block_size: int = 5,
                                                 samples_distance: int = 10):
    # compute weights for averaging an residual noise estimation
    block_size = np.minimum(epochs.shape[2], block_size)
    blocks = np.arange(0, epochs.shape[2] + 1, block_size).astype(np.int)
    _epochs = epochs[np.arange(0, epochs.shape[0], samples_distance), :, :]
    blocks[-1] = np.maximum(blocks[-1], _epochs.shape[2])
    unfolded_epochs = eo.et_unfold(np.transpose(_epochs, [0, 2, 1]), ortogonal=True)
    var = np.var(unfolded_epochs[:, blocks[0]: blocks[1]], axis=1)
    for i in range(1, len(blocks) - 1):
        var = np.vstack((var, np.var(unfolded_epochs[:, blocks[i]: blocks[i+1]], axis=1)))
    nb_var = np.mean(eo.et_fold(var.T, _epochs.shape[1]), axis=2)
    wb = np.ones((_epochs.shape[1], var.shape[0]))
    nk = np.diff(blocks)
    w = np.ones((_epochs.shape[1], nk[0])) * np.expand_dims(wb[:, 0], 1)
    for i, n in enumerate(nk[1:]):
        w = np.concatenate((w, np.ones(n) * np.expand_dims(wb[:, i + 1], 1)), axis=1)
    rn = np.sqrt(np.sum(nk * nb_var, axis=1) / (np.sum(nk) ** 2.0))
    cumulative_rn = None
    for _i in range(nb_var.shape[1]):
        _cum_rn = np.sqrt(np.sum(nk[0:_i + 1] * nb_var[:, 0:_i + 1], axis=1) / (np.sum(nk[0:_i + 1]) ** 2.0))
        if _i == 0:
            cumulative_rn = _cum_rn
        else:
            cumulative_rn = np.vstack((cumulative_rn, _cum_rn))
    return w, rn, cumulative_rn, nk


def et_mean(epochs: np.array = np.array([]),
            block_size=5,
            samples_distance=10,
            roi_windows: [np.array] = None,
            compute_cumulative_snr=False,
            weighted=True):

    if weighted:
        w, final_rn, cumulative_rn, nk = et_get_epoch_weights_and_rn_weighted_average(epochs,
                                                                                      block_size,
                                                                                      samples_distance)
    else:
        w, final_rn, cumulative_rn, nk = et_get_epoch_weights_and_rn_standard_average(epochs,
                                                                                      block_size,
                                                                                      samples_distance)

    w_ave = np.sum(epochs * w, axis=2) / np.sum(w, axis=1)
    final_snr, final_s_var = et_snr_in_rois(data=w_ave, roi_windows=roi_windows, rn=final_rn)
    n_trials = np.cumsum(nk).astype(int)
    cumulative_snr = []
    if compute_cumulative_snr:
        for _b in range(cumulative_rn.shape[0]):
            cum_w_ave = np.sum(
                epochs[:, :, 0: n_trials[_b]] * w[:, 0: n_trials[_b]], axis=2)
            c_snr, c_s_var = et_snr_in_rois(data=cum_w_ave, rn=cumulative_rn[_b, :], roi_windows=roi_windows)
            cumulative_snr.append(c_snr)
    fft = pyfftw.builders.rfft(w_ave, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
                               threads=multiprocessing.cpu_count())
    w_fft = fft()
    w_fft /= w_fft.shape[0]
    return w_ave, w, final_rn, cumulative_rn, final_snr, cumulative_snr, final_s_var, w_fft, nk


def et_snr_in_rois(data: np.array = np.array([]),
                   roi_windows: np.array = np.array([]),
                   rn: [np.array] = None):
    data_in_windows = get_in_window_data(data=data, roi_windows=roi_windows)
    if rn is None:
        rn = [np.array([])]
    final_snr = []
    final_s_var = []
    for _i, _data in enumerate(data_in_windows):
        s_var = np.var(_data, axis=0)
        snr = s_var / np.expand_dims(rn, 1) ** 2.0 - 1.0
        final_snr.append(np.atleast_1d(np.squeeze(snr.T)))
        final_s_var.append(np.atleast_1d(np.squeeze(s_var.T)))
    return final_snr, final_s_var


def get_in_window_data(data: np.array = np.array([]),
                       roi_windows: [np.array] = None):
    if roi_windows is None:
        snr_sample_windows = [np.array([0, data.shape[0]])]
    else:
        snr_sample_windows = roi_windows
    data_snr = []
    samples = []
    data = np.atleast_3d(data)
    for _i, _roi in enumerate(snr_sample_windows):
        _ini = np.maximum(_roi[0], 0)
        _end = np.minimum(_roi[1], data.shape[0])
        data_snr.append(data[_ini:_end, :, :])
        samples.append(np.array([_ini, _end]))
    return data_snr


def et_snr_weighted_mean(averaged_epochs: np.array = np.array([]),
                         rn: np.array = np.array([]),
                         snr: np.array = np.array([]),
                         roi_windows: [np.array] = None,
                         n_ch: int = None,
                         channel_idx: np.array = np.array([])):
    if not channel_idx.size:
        if n_ch is None:
            _v = np.arange(averaged_epochs.shape[1])
        else:
            _v = np.argsort(snr)[::-1][0:n_ch]
    else:
        _v = channel_idx
    _snr = snr
    if not np.any(snr):
        _snr = np.ones(snr.shape)
    w_ave = np.sum(averaged_epochs[:, _v] * _snr[_v].T, axis=1) / sum(_snr[_v])
    total_noise_var = rn[_v] ** 2.0
    w_total_noise_var = 1.0 / sum(1./total_noise_var)
    total_rn = w_total_noise_var ** 0.5
    total_snr, _signal_var = et_snr_in_rois(data=np.expand_dims(w_ave, 1), roi_windows=roi_windows, rn=total_rn)
    return np.expand_dims(w_ave, axis=1), total_rn, total_snr, _signal_var


def et_frequency_mean(epochs=np.array([]), fs=None,
                      samples_distance=10,
                      n_fft=None,
                      snr_time_window=np.array([]),
                      block_size=5,
                      test_frequencies=None,
                      scale_frequency_data=True):
    j_ave = JAve.JAverager()
    j_ave.splits = epochs.shape[1]
    j_ave.fs = fs
    j_ave.t_p_snr = np.arange(0, epochs.shape[0] / fs, epochs.shape[0] / samples_distance / fs)
    j_ave.analysis_window = snr_time_window
    j_ave.time_offset = 0.0
    j_ave.alpha_level = 0.05
    j_ave.min_block_size = block_size
    j_ave.plot_sweeps = False
    j_ave.frequencies_to_analyze = test_frequencies
    j_ave.n_fft = n_fft

    _n_sweep = 0
    _total_sweeps = epochs.shape[1] * epochs.shape[2]
    for i in range(epochs.shape[2]):
        for j in range(epochs.shape[1]):
            j_ave.add_sweep(epochs[:, j, i])
            _n_sweep += 1
            print(('averaged %i out of %i sweeps' % (_n_sweep, _total_sweeps)))
    w_ave = j_ave.w_average
    w_fft = j_ave.w_fft
    rn = j_ave.w_rn
    snr = j_ave.w_snr
    s_var = j_ave.w_signal_variance
    _h_test = j_ave.hotelling_t_square_test()
    if scale_frequency_data:
        w_fft /= epochs.shape[0] / 2
        for ch_tests in _h_test:
            for _s_test in ch_tests:
                _s_test.spectral_magnitude /= epochs.shape[0] / 2
                _s_test.rn /= epochs.shape[0] / 2
    return w_ave, rn, [snr], w_fft, _h_test, [s_var]


def et_subtract_correlated_ref(epochs: np.array = np.array([]),
                               ref: np.array = np.array([])):
    unfold_epochs = eo.et_unfold(epochs)
    unfold_ref = eo.et_unfold(ref)
    for _unfold_ref in unfold_ref.T:
        transmission_index = unfold_epochs.T.dot(_unfold_ref) / np.expand_dims(np.sum(np.square(_unfold_ref),
                                                                                      axis=0), axis=1)
        unfold_epochs -= np.expand_dims(_unfold_ref, axis=1).dot(np.expand_dims(transmission_index, axis=0))
    return eo.et_fold(unfold_epochs, epochs.shape[0])


def et_get_spatial_filtering(epochs: np.array = np.array([]),
                             fs: float = None,
                             sf_join_frequencies=None,
                             weighted_average=True):
    print('computing spatial filter')
    # bias function uses weighted mean or standard mean
    mean_data, w, _, _, _, _, _, w_fft, _ = et_mean(epochs, weighted=weighted_average)
    # mean_data = np.mean(epochs, axis=2)

    if not (sf_join_frequencies is None):
        c0, c1 = sf.nt_bias_fft(epochs, sf_join_frequencies / fs)
    else:
        c0 = eo.et_covariance(epochs)
        c1 = eo.et_covariance(np.expand_dims(mean_data, 2))

    todss, pwr0, pwr1 = sf.nt_dss0(c0, c1)
    z = eo.et_mmat(epochs, todss)
    cov_1 = eo.et_x_covariance(z, epochs)
    return z, pwr0, pwr1, cov_1


def et_apply_spatial_filtering(z=np.array([]),
                               pwr0=np.array([]),
                               pwr1=np.array([]),
                               cov_1=np.array([]),
                               sf_components=np.array([]),
                               sf_thr=0.8):
    print('applying spatial filter')
    if sf_components is None or not sf_components.size:
        p_ratio = pwr1 / pwr0
        pos = np.maximum(np.where(np.cumsum(p_ratio)/np.sum(p_ratio) >= sf_thr)[0][0], 1)
        n_components = np.arange(pos)
    else:
        n_components = sf_components
    return eo.et_mmat(z[:, n_components, :], cov_1[n_components, :]), n_components


def estimate_rn_from_trace(data=np.array([]), rn_ini_sample=0, rn_end_sample=-1, plot_estimation=False):
    x = np.expand_dims(np.arange(0, data[rn_ini_sample:rn_end_sample, :].shape[0]), axis=1)
    _subset = data[rn_ini_sample:rn_end_sample, :]
    regression = linear_model.LinearRegression()
    regression.fit(x, _subset)
    rn = np.std(_subset - regression.predict(x), axis=0)
    if plot_estimation:
        plt.plot(x, _subset)
        plt.plot(x, regression.predict(x))
        plt.title('rn regression, rn:' + str(rn))
        plt.show()

    return rn


def estimate_srn_from_trace(data=np.array([]), snr_ini_sample=0, snr_end_sample=-1, **kwargs):
    rn = estimate_rn_from_trace(data=data, **kwargs)
    _subset = data[snr_ini_sample:snr_end_sample, :]
    snr = np.var(_subset, axis=0) / rn ** 2.0 - 1
    return snr, rn


def eye_artifact_subtraction(epochs: np.array = np.array([]),
                             oeg_epochs: np.array = np.array([])):
    # subtract eye artifacts
    print('removing oeg artifacts')
    clean_data = et_subtract_correlated_ref(epochs=epochs, ref=oeg_epochs)
    return clean_data


def et_average_time_frequency_transformation(epochs: np.array = np.array([]),
                                             method='spectrogram',
                                             time_window=2.0,
                                             sample_interval=2.0,
                                             fs=0.0,
                                             average_mode='magnitude'):

    nfft = int(time_window * fs)
    noverlap = max(nfft - int(sample_interval * fs), 0)
    power, freqs, ave = None, None, None
    data = epochs
    if average_mode == 'complex':
        data = np.mean(epochs, 2, keepdims=True)

    for _i in range(data.shape[2]):
        if method == 'spectrogram':
            freqs, time, power = signal.spectrogram(data[:, :, _i],
                                                    window=signal.hamming(nfft),
                                                    fs=fs,
                                                    nfft=nfft,
                                                    noverlap=noverlap,
                                                    mode='complex',
                                                    scaling='spectrum',
                                                    axis=0)
            if average_mode == 'magnitude':
                power = np.abs(power)
        if method == 'wavelet':
            freq = np.arange(1, data.shape[0] + 1) * fs / data.shape[0]
            mother = wavelet.Morlet(f0=2 * np.pi)
            s0 = 2 / fs  # Starting scale
            dj = 1 / 12.  # Twelve sub-octaves per octaves
            J = 7 / dj  # Seven powers of two with dj sub-octaves
            [cfs, freqs] = pw.cwt(data[:, :, _i],
                                  scales=np.arange(1, 128),
                                  wavelet='cmor',
                                  sampling_period=1 / fs,
                                  axis=0)

            w = pw.ContinuousWavelet('cmor')
            w.bandwidth_frequency = 1.5
            w.center_frequency = 1
            [cfs, freqs] = pw.cwt(data,
                                  scales=np.arange(1, data.shape[0] / 2),
                                  wavelet=w,
                                  sampling_period=1 / fs)
            wave, scales, freqs, coi, fft, fftfreqs = wavelet.cwt(data,
                                                                  dt=1 / fs,
                                                                  freqs=freq)
            # power = (np.abs(wave)) ** 2
            # fft_power = np.abs(fft) ** 2
            # period = 1 / freqs
            power = np.abs(cfs)

        if _i == 0:
            ave = power
        else:
            ave += power
        print('Computing spectrogram {:} out of {:}'. format(_i, data.shape[2]))
    ave /= data.shape[2]
    return ave, time, freqs


def et_average_frequency_transformation(epochs: np.array = np.array([]),
                                        fs=0.0,
                                        ave_mode='magnitude'):

    fft = pyfftw.builders.rfft(epochs, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
                               threads=multiprocessing.cpu_count())
    _fft = fft()

    if ave_mode == 'magnitude':
        _fft = np.abs(_fft)
    ave = np.abs(np.mean(_fft, axis=2))
    ave *= 2/epochs.shape[0]
    freqs = np.arange(0, _fft.shape[0]) * fs / epochs.shape[0]
    return ave, freqs


def bootstrap(data: np.array = np.array([]), statistic=np.mean, num_samples=1000, alpha=0.05):
    """
    This function bootstrap incoming data using passed statistic function
    :param data: numpy array
    :param statistic: function that will be bootstrapped
    :param num_samples: number of estimations that will be used
    :param alpha: probabilistic level to compute confidence intervals
    :param axis: axis where statistic will be applied
    :return: bootstrap estimate of 100.0*(1-alpha) CI for statistic.
    """
    n = data.shape[2]
    idx = np.random.randint(0, n, (int(num_samples), n))
    unfolded_epochs = eo.et_unfold(np.transpose(data, [0, 2, 1]), ortogonal=True)
    new_data = np.empty((unfolded_epochs.shape[0], num_samples))
    for _set in range(num_samples):
        samples = unfolded_epochs[:, idx[_set]]
        new_data[:, _set] = statistic(samples, axis=1)

    stat = np.sort(new_data, axis=1)
    mean = np.mean(stat, axis=1)
    ci_low, ci_hi = (stat[:, int((alpha / 2.0) * num_samples)], stat[:, int((1 - alpha / 2.0) * num_samples)])
    out_mean = np.reshape(mean, (data.shape[0], data.shape[1]), order='F')
    out_ci_low = np.reshape(ci_low, (data.shape[0], data.shape[1]), order='F')
    out_ci_hi = np.reshape(ci_hi, (data.shape[0], data.shape[1]), order='F')
    return out_mean, out_ci_low, out_ci_hi
