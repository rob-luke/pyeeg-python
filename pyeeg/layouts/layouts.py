import os
from os.path import sep
import logging
import numpy as np
from pyeeg.definitions.channel_definitions import ChannelItem
__author__ = 'jundurraga'
log = logging.getLogger()


class Layout(object):
    def __init__(self, file_name='biosemi64.lay'):
        self.layout = self.get_layout(file_name)

    def get_layout(self, file_name='biosemi64.lay'):
        _path = None
        if file_name is not None:
            _path = os.path.dirname(os.path.realpath(__file__)) + sep + file_name
        return self.read_layout(_path)

    @staticmethod
    def read_layout(file_path=None):
        out = np.array([])
        if file_path is not None:
            with open(file_path, 'r') as f:
                while True:
                    _data = f.readline()
                    if not _data:
                        break
                    lay = _data.split()
                    out = np.append(out,
                                    ChannelItem(idx=int(lay[0]) - 1,
                                                x=float(lay[1]),
                                                y=float(lay[2]),
                                                w=float(lay[3]),
                                                h=float(lay[4]),
                                                label=lay[5]))
        return out

    def get_labels(self):
        return np.array([_ch.label for _ch in self.layout])

    def get_index(self):
        return np.array([_ch.idx for _ch in self.layout])

    def get_item(self, idx=None):
        _idx = max(0, min(idx, self.layout.size))
        return self.layout[_idx]

    @staticmethod
    def change_layout_idem(layout={}, labels_to_replace=[['from', 'to']], channel_mapping=[]):
        for _pair in labels_to_replace:
            _l_from = _pair[0]
            _l_to = _pair[1]
            _pos_ch_from = [(_pos, layout['channel'][_pos]) for _pos, _ch in enumerate(layout['label']) if _ch == _l_from]
            _pos_ch_to = [(_pos, layout['channel'][_pos]) for _pos, _ch in enumerate(layout['label']) if _ch == _l_to]

            if _pos_ch_from:
                number_in_map_from = [(_pos, _ch['number']) for _pos, _ch in enumerate(channel_mapping) if
                                      _ch['number'] == _pos_ch_from[0][1]]
            else:
                number_in_map_from = [(_pos, _ch['number']) for _pos, _ch in enumerate(channel_mapping) if _ch['label'] == _l_from]

            if _pos_ch_to:
                number_in_map_to = [(_pos, _ch['number']) for _pos, _ch in enumerate(channel_mapping) if
                                    _ch['number'] == _pos_ch_to[0][1]]
            else:
                number_in_map_to = [(_pos, _ch['number']) for _pos, _ch in enumerate(channel_mapping) if _ch['label'] == _l_to]

            if number_in_map_from and number_in_map_to:
                ini_channel = number_in_map_from[0][1]
                end_channel = number_in_map_to[0][1]
                if _pos_ch_from:
                    _pos_from = _pos_ch_from[0][0]
                    layout['channel'][_pos_from] = end_channel
                else:
                    channel_mapping[number_in_map_from[0][0]]['number'] = end_channel

                text = 'remapping channel {:s} to {:s}'.format(_l_from, _l_to)
                logging.info('\n' + ''.join(text))
                print(text)

                if _pos_ch_to:
                    _pos_to = _pos_ch_to[0][0]
                    layout['channel'][_pos_to] = ini_channel
                else:
                    channel_mapping[number_in_map_to[0][0]]['number'] = ini_channel

                text = 'remapping channel {:s} to {:s}'.format(_l_to, _l_from)
                logging.info('\n' + ''.join(text))
                print(text)
            else:
                text = 'could not remap channel {:s} by {:s}, one of the electrode pairs is not in the map'.format(_l_to, _l_from)
                logging.info('\n' + ''.join(text))
                print(text)

