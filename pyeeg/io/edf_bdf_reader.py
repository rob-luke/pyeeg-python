import os
import numpy as np
import re
from pyeeg.definitions.channel_definitions import ChannelItem, ChannelType
import logging
import astropy.units as u
log = logging.getLogger()


class DeviceEventChannel:
    bdf_event_channel = 'Status'
    edf_event_channel = 'EDF Annotations'


def read_edf_bdf_header(file_name):
    with open(file_name, 'rb') as f:
        # read first 256 bytes
        # first byte is skipped as is not important for bdf or edf files
        f.seek(1, 1)
        header = {'file_name': file_name,
                  'file_size': os.path.getsize(file_name),
                  'version': f.read(7).strip().decode('ascii'),
                  'subject_id': f.read(80).strip().decode('ascii'),
                  'recording_id': f.read(80).strip().decode('ascii'),
                  'start_date': f.read(8).strip().decode('ascii'),
                  'start_time': f.read(8).strip().decode('ascii'),
                  'bytes_in_header': int(f.read(8)),
                  'data_format': f.read(44).strip().decode('ascii'),
                  'number_records': int(f.read(8)),
                  'duration_data_record': float(f.read(8)),
                  'n_channels': int(f.read(4))}
        header['duration'] = header['number_records'] * header['duration_data_record']
        # read next N x 256 bytes
        ch = []
        [ch.append(ChannelItem(label=f.read(16).strip().decode('ascii'), idx=i)) for i in range(header['n_channels'])]
        header['channels'] = np.array(ch)
        ch = []
        [ch.append(f.read(80).strip().decode('ascii')) for i in range(header['n_channels'])]
        header['transducer'] = np.array(ch)
        ch = []
        [ch.append(f.read(8).strip().decode('ascii')) for i in range(header['n_channels'])]
        header['physical_dimension'] = np.array(ch)
        ch = []
        [ch.append(float(f.read(8))) for i in range(header['n_channels'])]
        header['physical_minimum'] = np.array(ch)
        ch = []
        [ch.append(float(f.read(8))) for i in range(header['n_channels'])]
        header['physical_maximum'] = np.array(ch)
        ch = []
        [ch.append(float(f.read(8))) for i in range(header['n_channels'])]
        header['digital_minimum'] = np.array(ch)
        ch = []
        [ch.append(float(f.read(8))) for i in range(header['n_channels'])]
        header['digital_maximum'] = np.array(ch)
        ch = []
        [ch.append((header['physical_maximum'][i] - header['physical_minimum'][i]) /
                   (header['digital_maximum'][i] - header['digital_minimum'][i]))
         for i in range(header['n_channels'])]
        header['gain'] = np.array(ch)
        ch = []
        [ch.append(f.read(80).strip().decode('ascii')) for i in range(header['n_channels'])]
        header['pre_filtering'] = np.array(ch)
        ch = []
        [ch.append(int(f.read(8))) for i in range(header['n_channels'])]
        header['number_samples_per_record'] = np.array(ch)
        ch = []
        [ch.append(f.read(32).strip().decode('ascii')) for i in range(header['n_channels'])]
        header['reserved'] = np.array(ch)
        ch = []
        [ch.append(header['number_samples_per_record'][i] / header['duration_data_record'])
         for i in range(header['n_channels'])]
        header['fs'] = np.array(ch)
        if header['number_records'] == -1:
            header_off_set = 256 * (1 + header['n_channels'])
            header['number_records'] = ((os.stat(header['file_name']).st_size -
                                         header_off_set) / header['n_channels'] / header['fs'][0] / 3).astype(np.int64)
            header['duration'] = header['number_records'] * header['duration_data_record']

    return header


def read_bdf_channel(header, channels_idx: np.array = np.array([]),
                     ini_time=0,
                     end_time: float = None,
                     include_event_channel=True,
                     event_channel_label=DeviceEventChannel.bdf_event_channel):
    fs = header['fs'][0]
    if event_channel_label is None:
        event_channel_label = DeviceEventChannel.bdf_event_channel
    if end_time is None:
        end_time = np.floor(header['duration'])
    else:
        end_time = np.minimum(end_time, header['duration'])

    if not channels_idx.size:
        channels = header['channels']
    else:
        channels = header['channels'][channels_idx]
    if include_event_channel:
        # check if trigger channel is in channel list
        _idx_event = np.squeeze([_i for _i, _ch in enumerate(channels) if _ch.label == event_channel_label])
        if _idx_event.size == 0:
            _idx_event = [_i for _i, _ch in enumerate(header['channels']) if _ch.label == event_channel_label][0]
            channels = np.append(channels, header['channels'][_idx_event])

    ini_time = np.round(ini_time).astype(np.int64)
    records_to_read = np.floor(end_time - ini_time).astype(np.int64)
    buffer_size = np.round(records_to_read * fs).astype(np.int64)
    start_offset = 3 * ini_time * np.sum(header['number_samples_per_record']).astype(np.int64)
    data_channel = np.zeros((buffer_size, len(channels) - 1), dtype=np.float32)
    event_channel = np.empty((buffer_size, 1), dtype=np.float32)
    sys_code_channel = np.zeros((buffer_size,), dtype=np.float)
    record_length = np.int64(np.sum(header['number_samples_per_record']) * 3)
    _time = ini_time
    with open(header['file_name'], 'rb') as f:
        # skip first 256 bytes + 256 * N channels
        header_off_set = 256 * (1 + header['n_channels'])
        for nr in range(records_to_read):
            for i, ch in enumerate(channels):
                samples_per_record = np.int64(header['number_samples_per_record'][ch.idx])
                f.seek(header_off_set + start_offset + samples_per_record * ch.idx * 3 +
                       record_length * nr)
                data = np.fromfile(f, dtype=np.uint8, count=3 * samples_per_record)
                try:
                    data = data.reshape(-1, 3).astype(np.int32)
                except Exception:
                    print("Your data file seems to be INCOMPLETE, check it!")
                    break
                if not data.size:
                    break
                if ch.label != event_channel_label:
                    data = np.int32((data[:, 0] << 8) | (data[:, 1] << 16) | (data[:, 2] << 24)) >> 8
                    data_channel[nr * samples_per_record: (nr + 1) * samples_per_record, i] = data
                else:
                    sys_code_channel[nr * samples_per_record: (nr + 1) * samples_per_record] = data[:, 2]
                    data = (data[:, 0] | (data[:, 1] << 8)) & 255
                    event_channel[nr * samples_per_record: (nr + 1) * samples_per_record, 0] = data
                    # set channel type to event
                    ch.type = ChannelType.Event
            _time += header['duration_data_record']
            print('Processed time: {:.1f}'.format(_time))
    gain = np.array([header['gain'][ch.idx] for ch in channels])
    return data_channel * gain[0:-1], event_channel * gain[-1]


def read_edf_channel(header,
                     channels_idx: np.array = np.array([]),
                     ini_time=0,
                     end_time: float = None,
                     include_event_channel=True,
                     event_channel_label=DeviceEventChannel.edf_event_channel):
    fs = header['fs'][0]
    if event_channel_label is None:
        event_channel_label = DeviceEventChannel.edf_event_channel
    if end_time is None:
        end_time = np.floor(header['duration'])
    else:
        end_time = np.minimum(end_time, header['duration'])
    if not channels_idx.size:
        channels = header['channels']
    else:
        channels = header['channels'][channels_idx]
    _idx_event = None
    if include_event_channel:
        # check if trigger channel is in channel list
        _idx_event = np.squeeze(np.array([_i for _i, _ch in enumerate(channels) if _ch.label == event_channel_label]))
        if _idx_event.size == 0:
            _idx_event = [_i for _i, _ch in enumerate(header['channels']) if _ch.label == event_channel_label][0]
            channels = np.append(channels, header['channels'][_idx_event])

    ini_time = round(ini_time)
    records_to_read = np.floor((end_time - ini_time) / header['duration_data_record']).astype(np.int64)
    buffer_size = np.round(records_to_read * header['duration_data_record'] * fs).astype(np.int64)
    start_offset = (2 * ini_time * np.sum(header['number_samples_per_record'])).astype(np.int64)
    data_channel = np.zeros((buffer_size, len(channels) - 1), dtype=np.float32)
    event_channel = np.empty((buffer_size, 1), dtype=np.float32)
    sys_code_channel = np.zeros((buffer_size,), dtype=np.int16)
    record_length = np.sum(header['number_samples_per_record']) * 2
    _time = ini_time
    with open(header['file_name'], 'rb') as f:
        # skip first 256 bytes + 256 * N channels
        header_off_set = 256 * (1 + header['n_channels'])
        for nr in range(records_to_read):
            for i, ch in enumerate(channels):
                samples_per_record = header['number_samples_per_record'][ch.idx]
                f.seek(header_off_set + start_offset + samples_per_record * ch.idx * 2 +
                       record_length * nr)
                data = np.fromfile(f, dtype=np.int16, count=samples_per_record)
                if ch.label != (event_channel_label or DeviceEventChannel.edf_event_channel):
                    data_channel[nr * samples_per_record: (nr + 1) * samples_per_record, i] = data
                if ch.label == (event_channel_label or DeviceEventChannel.edf_event_channel):
                    sys_code_channel[nr * samples_per_record: (nr + 1) * samples_per_record] = data
                    # set channel type to event
                    ch.type = ChannelType.Event
            _time += header['duration_data_record']
            print('Processed time %f' % _time)

    gain = np.array([header['gain'][ch.idx] for ch in channels])
    if include_event_channel:
        if channels[_idx_event].label == (event_channel_label or DeviceEventChannel.edf_event_channel):
            # fist we try getting annotated channel
            pattern = re.compile(r'([-+]\d*\.*\d*)(?:\\x00*)(?:\\x15*)([-+]\d*\.*\d*)?(?:\\x14)(.*?)(?:\\x14\\x00+)')
            start_event = pattern.findall(sys_code_channel.tostring().decode("utf-8"))
            used_events = [ev for ev in start_event if not ev[2] == '']

            if len(used_events) > 0:
                event_pos = []
                event_dur = []
                event_code = []

                for _i_time, _dur, _code in used_events:
                    event_pos.append(np.round(float(_i_time) * fs).astype(np.int64))
                    event_dur.append(1 if _dur == '' else np.round(float(_dur) * fs).astype(np.int64))
                    event_code.append(float(_code))

                for _ini, _dur, _code in zip(event_pos, event_dur, event_code):
                    vect = np.arange(_ini, _ini + _dur).astype(np.int64)
                    event_channel[vect, 1] = _code / gain[_idx_event]
            else:
                # if none annotations are found we try to read channel  event as a data channel
                event_channel[:, 1] = sys_code_channel / gain[_idx_event]

    return data_channel * gain[0:-1], event_channel * gain[-1]


def get_event_channel(header, ini_time=0, end_time=None, event_channel_label=None):
    event_channel, event_table, sys_code_channel = (None, None, None)
    _, file_extension = os.path.splitext(header['file_name'])
    if event_channel_label is None:
        event_channel_label = DeviceEventChannel.bdf_event_channel if file_extension == '.bdf' \
            else DeviceEventChannel.edf_event_channel
    if file_extension == '.bdf':
        _, event_channel = read_bdf_channel(header=header,
                                            ini_time=ini_time,
                                            end_time=end_time,
                                            event_channel_label=event_channel_label)
    if file_extension == '.edf':
        _, event_channel = read_edf_channel(header=header,
                                            ini_time=ini_time,
                                            end_time=end_time,
                                            event_channel_label=event_channel_label)

    return event_channel


def get_data(header, channels_idx=None, ini_time=0, end_time=None, event_channel_label=None):
    data, events, units = None, None, None
    _, file_extension = os.path.splitext(header['file_name'])
    if file_extension == '.bdf':
        units = u.uV
        data, events = read_bdf_channel(header=header, channels_idx=channels_idx,
                                        ini_time=ini_time, end_time=end_time,
                                        event_channel_label=event_channel_label)
    if file_extension == '.edf':
        units = u.uV
        data, events = read_edf_channel(header=header, channels_idx=channels_idx,
                                        ini_time=ini_time, end_time=end_time,
                                        event_channel_label=event_channel_label)
    return data, events, units
