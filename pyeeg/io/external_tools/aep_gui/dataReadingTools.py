from pyeeg.io.eeg import reader
from os import listdir, walk
from os.path import isfile, isdir, join, basename, sep
import pyeeg.definitions.edf_bdf_reader as ebreader
import numpy as np
from prettytable import PrettyTable
import logging
import datetime
from pyeeg.io.external_tools.aep_gui import extsys_tools as et
from pyeeg.processing.events import event_tools as tt
from pyeeg.io.external_tools.file_tools import DataLinks
from pyeeg.io.external_tools.file_tools import match_keywords
__author__ = 'jaime undurraga'


def get_measurement_and_triggers(measurement_path, **kwargs):
    data_files = kwargs.get('data_files', [])
    epoch_code = kwargs.get('epoch_code', None)
    measurement_code = kwargs.get('measurement_code', None)
    pair_event_idx = kwargs.get('pair_event_idx', None)
    merge_trigger_code = kwargs.get('merge_trigger_code', [])
    get_all_epoch_triggers = kwargs.get('get_all_epoch_triggers', False)
    pair_to_measurement_file = kwargs.get('pair_to_measurement_file', True)
    split_triggers_by_std = kwargs.get('split_triggers_by_std', False)
    relative_time_positions = kwargs.get('relative_time_positions', np.array([]))
    new_code_events = kwargs.get('new_code_events', np.array([]))
    trigger_channels = kwargs.get('trigger_channels', None)
    merge_similar_events = kwargs.get('merge_similar_events', False)
    logging.basicConfig(filename=measurement_path + sep + 'log_file.log', filemode='w', level=logging.INFO)
    logger = logging.getLogger()
    logger.handlers[0].stream.close()
    logger.removeHandler(logger.handlers[0])
    file_handler = logging.FileHandler(filename=measurement_path + sep + 'log_file.log', mode='w')
    logger.addHandler(file_handler)
    pairs = []
    if not data_files:
        data_files = [join(measurement_path, f) for f in listdir(measurement_path)
                      if isfile(join(measurement_path, f)) and (f.endswith('.bdf') or f.endswith('.edf'))]
    parameter_files = [join(measurement_path, f) for f in listdir(measurement_path)
                       if isfile(join(measurement_path, f)) and f.endswith('.extsys')]

    if len(parameter_files) == 0 or not pair_to_measurement_file:
        # get data without sort bdf files
        data = []
        for i, _file_name in enumerate(data_files):
            data.append(ebreader.EdfBdfDataReader(file_name=_file_name, **kwargs))
        for _, _data in enumerate(data):
            triggers = _data.get_triggers_events(**kwargs)
            if epoch_code is not None and relative_time_positions.size and new_code_events.size:
                triggers, epoch_code = _data.add_trigger_events(triggers=triggers,
                                                                master_triggers=epoch_code,
                                                                relative_time_positions=relative_time_positions,
                                                                new_code_events=new_code_events)
            unique_events = np.unique(triggers['code'])
            event_counter = []
            for j, code in enumerate(unique_events):
                event_counter.append({'code': code, 'n': len(np.where(triggers['code'] == code)[0])})
            logging.info("\n".join([_file_name, 'Trigger events:', str(event_counter)]))

            # first merge trigger codes
            if merge_trigger_code:
                triggers = _data.merge_triggers(triggers=triggers, trigger_code=merge_trigger_code)

            # set maximum trigger code as epoch code
            if epoch_code is None:
                epoch_code = [max(unique_events)]
            _message = 'Using event %s as epoch code' % epoch_code
            logging.info(_message)
            print(_message)

            # split triggers
            if split_triggers_by_std:
                triggers = _data.split_triggers_by_std(triggers=triggers, epoch_code=epoch_code)
            elif get_all_epoch_triggers:
                triggers = _data.get_all_triggers(trigger_data=triggers, trigger_codes=epoch_code)
            else:
                triggers = _data.get_sub_triggers(trigger_data=triggers, trigger_codes=epoch_code)

            # check for a minimum number of triggers
            triggers = _data.clean_triggers(triggers, **kwargs)

            if len(triggers['triggers']) > 0:
                for i in range(len(triggers['triggers'])):
                    pairs.append({'data_reader': ebreader.EdfBdfDataReader(file_name=data_files[0], **kwargs),
                                  'parameters_file_name': '',
                                  'triggers': triggers['triggers'][i],
                                  'parameters': None})
            else:
                pairs.append({'data_reader': _data,
                              'parameters_file_name': '',
                              'triggers': triggers,
                              'parameters': None})
        t = PrettyTable()
        t.add_column(fieldname='bdf file name', column=[basename(x) for x in data_files])
        logging.info(t)
        print(t)
    elif len(data_files) == len(parameter_files):
        # get and sort parameters dates
        parameters = []
        for i, _file_name in enumerate(parameter_files):
            parameters.append(et.get_measurement_info_from_zip(_file_name))
        dates = [par['Measurement']['MeasurementModule']['Date'] for x, par in enumerate(parameters)]
        idx_par = [i[0] for i in sorted(enumerate(dates),
                                        key=lambda xx: datetime.datetime.strptime(xx[1], '%m-%d-%y-%H-%M-%S-%f'))]

        # get and sort bdf files dates
        _data = []
        for i, _file_name in enumerate(data_files):
            _data.append(ebreader.EdfBdfDataReader(file_name=_file_name, **kwargs))
        file_dates = [_c_data._header['start_date'] + '.' + _c_data._header['start_time']
                      for x, _c_data in enumerate(_data)]
        idx_bdf = [i[0] for i in sorted(enumerate(file_dates), key=lambda xx: xx[1])]
        for i_p, i_b in zip(idx_par, idx_bdf):
            if find_file_name_flag(parameter_files[i_p], '_bad_'):
                continue
            if trigger_channels is not None:
                ideal_epoch_length = parameters[i_p]['Measurement']['StimuliModule']['Stimulus'][1]['Parameters']['Duration']
                kwargs.setdefault('ideal_epoch_length', ideal_epoch_length)
            triggers = _data[i_b].get_triggers_events(**kwargs)
            unique_events = np.unique(triggers['code'])
            event_counter = []
            for j, code in enumerate(unique_events):
                event_counter.append({'code': code, 'n': len(np.where(triggers['code'] == code)[0])})
            logging.info("\n".join([_file_name, 'Trigger events:', str(event_counter)]))

            # first merge trigger codes
            if merge_trigger_code:
                triggers = _data[i_b].merge_triggers(triggers=triggers, trigger_code=merge_trigger_code)

            # find possible events
            min_event_diff = np.inf
            pos_event = 0
            for i, event in enumerate(event_counter):
                if np.abs(event['n'] - len(parameter_files)) <= min_event_diff:
                    min_event_diff = np.abs(event['n'] - len(parameter_files))
                    pos_event = i
            subset_code = event_counter[pos_event]['code']
            if epoch_code is None:
                _epoch_event_candidates = [ev for i, ev in enumerate(event_counter)
                                           if ev['code'] != subset_code and ev['n'] >= len(parameter_files)]
                epoch_code = np.array([max([ev['code'] for ev in _epoch_event_candidates])])

            _message = 'Using event %s as epoch code' % epoch_code
            logging.info(_message)
            print(_message)

            triggers = _data[i_b].get_sub_triggers(trigger_data=triggers, trigger_codes=epoch_code)

            pairs.append({'data_reader': _data[i_b],
                          'parameters_file_name': parameter_files[i_p],
                          'triggers': triggers,
                          'parameters': parameters[i_p]})
        t = PrettyTable()
        t.add_column(fieldname='File name', column=[basename(parameter_files[x]) for x in idx_par])
        t.add_column(fieldname='File date', column=[dates[x] for x in idx_par])
        t.add_column(fieldname='bdf file name', column=[basename(data_files[x]) for x in idx_bdf])
        t.add_column(fieldname='bdf file date', column=[file_dates[x] for x in idx_bdf])
        logging.info(t)
        print(t)
    elif len(data_files) == 1:
        data_master = ebreader.EdfBdfDataReader(file_name=data_files[0], **kwargs)
        triggers = data_master.get_triggers_events(**kwargs)

        if split_triggers_by_std:
            triggers = data_master.split_triggers_by_std(triggers=triggers, epoch_code=epoch_code)

        unique_events = np.unique(triggers['code'])
        event_counter = []
        for i, code in enumerate(unique_events):
            event_counter.append({'code': code, 'n': len(np.where(triggers['code'] == code)[0])})
        logging.info("\n".join([data_files[0], 'Trigger events:', str(event_counter)]))

        # first merge trigger codes
        if merge_trigger_code:
            triggers = data_master.merge_triggers(triggers=triggers, trigger_code=merge_trigger_code)

        # find events that match as closet as possible the number of parameter files
        if measurement_code is None:
            pos_event = 0
            best_match = np.inf
            for i, event in enumerate(event_counter):
                if np.abs(event['n'] - len(parameter_files)) < best_match:
                    # min_event_diff = np.abs(event['n'] - len(parameter_files))
                    pos_event = i
                    best_match = np.abs(event['n'] - len(parameter_files))
        else:
            for i, event in enumerate(event_counter):
                if event['code'] == measurement_code:
                    pos_event = i
                    break
        subset_code = event_counter[pos_event]['code']
        _message = 'Using event code %i to pair measurement files' % subset_code
        logging.info(_message)
        print(_message)

        if not epoch_code:
            _epoch_event_candidates = [ev for i, ev in enumerate(event_counter)
                                       if ev['code'] != subset_code and ev['n'] >= len(parameter_files)]
            epoch_code = max([ev['code'] for ev in _epoch_event_candidates])
        _message = 'Using %s as epoch code' % epoch_code
        logging.info(_message)
        print(_message)

        assert subset_code is not None, 'Could not find a trigger code matching the number of parameter files'
        if not split_triggers_by_std:
            triggers = data_master.get_sub_triggers(trigger_data=triggers, trigger_codes=[subset_code, epoch_code])
        # check for a minimum number of triggers
        triggers = data_master.clean_triggers(triggers, **kwargs)

        sorted_parameters, sorted_parameter_files, dates, idx_par = et.get_extsys_parameters(parameter_files)
        t = PrettyTable()
        t.add_column(fieldname='File name', column=[basename(parameter_files[x]) for x in idx_par])
        t.add_column(fieldname='File date', column=[dates[x] for x in idx_par])
        t.add_column(fieldname='Trigger set', column=list(range(len(idx_par))))
        logging.info(t)
        print(t)
        if not pair_event_idx:
            pair_event_idx = list(range(len(triggers['triggers'])))

        if merge_similar_events:
            sorted_parameters, indexes, inverse = et.merge_parameters_by_condition(sorted_parameters, **kwargs)
            sorted_parameter_files = sorted_parameter_files[indexes]
            _temp_triggers = np.array(triggers['triggers'])
            triggers['triggers'] = [None] * len(indexes)
            for _idx in indexes:
                _cond_idx = np.where(inverse == _idx)[0]
                _triggers = tt.join_triggers(_temp_triggers[_cond_idx])
                triggers['triggers'][_idx] = _triggers
            pair_event_idx = list(range(len(triggers['triggers'])))

        for i, (s_p, s_p_f) in enumerate(zip(sorted_parameters, sorted_parameter_files)):
            if find_file_name_flag(s_p_f, '_bad_'):
                continue
            pairs.append({'data_reader': ebreader.EdfBdfDataReader(file_name=data_files[0], **kwargs),
                          'parameters_file_name': s_p_f,
                          'triggers': triggers['triggers'][pair_event_idx[i]],
                          'parameters': s_p})

    else:
        _message = 'The number of bdf files must be either 1 or the same amount of .extsys files!'
        logging.error(_message)
        print(_message)

    return pairs


def find_bdf_directories(root_path, f_type='.bdf'):
    out = []
    # search on root path
    root_data_files = [join(root_path, f) for f in listdir(root_path) if
                       isfile(join(root_path, f)) and f.endswith(f_type)]
    if root_data_files:
        out.append(root_path)
    # search in subdirectories
    _directories = [join(root_path, d) for d in listdir(root_path) if isdir(join(root_path, d))]
    for _dir in _directories:
        _out = find_bdf_directories(_dir)
        if _out:
            [out.append(_path) for _path in _out]
        data_files = [join(_dir, f) for f in listdir(_dir) if isfile(join(_dir, f)) and f.endswith(f_type)]
        if data_files:
            out.append(_dir)

    return set(out)


def events_summary(events: np.array([tt.SingleEvent]) = None):
    _codes = np.array([_e.code for _e in events])
    unique_events = np.unique(_codes)
    event_counter = []
    for i, code in enumerate(unique_events):
        event_counter.append({'code': code, 'n': len(np.where(_codes == code)[0])})
    logging.info("\n".join(['Trigger events:', str(event_counter)]))
    print("\n".join(['Trigger events:', str(event_counter)]))


def get_files_and_meta_data(measurement_path: str = '',
                            split_trigger_code: float = None,
                            filter_key_words: [str] = None,
                            exclude_key_words: [str] = None,
                            deep_match=True
                            ) -> [DataLinks]:
    """
    This function search for .bdf or .edf  and .extsys files recursively and try to pair them according to their date.
    The result is a list of DataLinks object, containing the information that links both files.
    :param measurement_path: the root path to search for pairs of files
    :param split_trigger_code: if provided, each .extsys file will be matched to the same bdf file, the ini_time and
    end_time of the DataLinks files will be determined by the trigger_code passed.
    :param filter_key_words: if provided, the return list will only contain DataLinks object whose paths do include
    the filter_key_words. This is useful to process entire folders with different conditions
    :param exclude_key_words: if provided, the return list will only contain DataLinks object whose paths do not include
    the exclude_key_words. This is useful to process entire folders with different conditions
    :param deep_match: if True, all keywords must be present to return True
    :return: a list of DataLinks objects which contain the information linking .bdf or .edf and .extsys files
    """
    par_out = []
    _all_directories = [x[0] for x in walk(measurement_path)]
    for _current_directory in _all_directories:
        if filter_key_words is not None and not match_keywords(filter_key_words, _current_directory,
                                                               deep_match=deep_match):
            continue
        if exclude_key_words is not None and match_keywords(exclude_key_words, _current_directory,
                                                            deep_match=deep_match):
            continue

        data_files = [join(_current_directory, f) for f in listdir(_current_directory)
                      if isfile(join(_current_directory, f)) and (f.endswith('.bdf') or f.endswith('.edf'))]
        parameter_files = [join(_current_directory, f) for f in listdir(_current_directory)
                           if isfile(join(_current_directory, f)) and f.endswith('.extsys')]

        # get and sort parameters dates
        parameters = []
        for i, _file_name in enumerate(parameter_files):
            parameters.append(et.get_measurement_info_from_zip(_file_name))
        dates = [par['Measurement']['MeasurementModule']['Date'] for x, par in enumerate(parameters)]
        idx_par = [_i[0] for _i in sorted(enumerate([datetime.datetime.strptime(_date, '%m-%d-%y-%H-%M-%S-%f')
                                                     for _date in dates]), key=lambda x: x[1])]
        # get and sort bdf files dates
        _data = []
        for i, _file_name in enumerate(data_files):
            _eeg_reader = reader.eeg_reader(_file_name)
            _data.append(_eeg_reader)
        file_dates = [_c_data._header['start_date'] + '.' + _c_data._header['start_time']
                      for x, _c_data in enumerate(_data)]
        idx_bdf = [_i[0] for _i in sorted(enumerate([datetime.datetime.strptime(_date, '%d.%m.%y.%H.%M.%S')
                                                     for _date in file_dates]), key=lambda x: x[1])]
        # initialize ini and end time to read raw data
        ini_time, end_time = [0] * len(idx_par), [None] * len(idx_bdf)
        if len(idx_bdf) == 1 and split_trigger_code is not None:
            # ensure each parameter file and data file have are paired
            idx_bdf = idx_bdf * len(idx_par)
            raw_events = _data[0].get_events()
            events = tt.detect_events(event_channel=raw_events, fs=_data[0].fs)
            events_summary(events)
            _split_events = np.array([_e for _e in events if _e.code == split_trigger_code])
            assert _split_events.size == len(idx_par), "number of triggers to split conditions does not match the number" \
                                                       "condition files"
            ini_time = [_e.time_pos for _e in _split_events]
            end_time = [_e.time_pos for _e in _split_events[1:]]
            end_time.append(None)

        for i_p, i_b, _ini_time, _end_time in zip(idx_par, idx_bdf, ini_time, end_time):
            if filter_key_words is not None and \
                    not (match_keywords(filter_key_words, _data[i_b].file_name, deep_match=deep_match) and
                         match_keywords(filter_key_words, parameter_files[i_p], deep_match=deep_match)):
                continue
            if exclude_key_words is not None and \
                    (match_keywords(exclude_key_words, parameter_files[i_p], deep_match=deep_match) or
                     match_keywords(exclude_key_words, parameter_files[i_p], deep_match=deep_match)):
                continue
            if match_keywords(['_bad_'], parameter_files[i_p]):
                continue

            data_links = DataLinks(parameters=parameters[i_p],
                                   parameters_file=parameter_files[i_p],
                                   parameters_date=dates[i_p],
                                   data_file=_data[i_b].file_name,
                                   data_date=file_dates[i_b],
                                   ini_time=_ini_time,
                                   end_time=_end_time
                                   )
            par_out.append(data_links)

    t = PrettyTable()
    t.add_column(fieldname='File name', column=[basename(x.parameters_file) for x in par_out])
    t.add_column(fieldname='File date', column=[basename(x.parameters_date) for x in par_out])
    t.add_column(fieldname='bdf file name', column=[basename(x.data_file) for x in par_out])
    t.add_column(fieldname='bdf file date', column=[x.data_date for x in par_out])
    logging.info(t)
    print(t)
    return par_out
