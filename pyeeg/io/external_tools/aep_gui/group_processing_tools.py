import pyeeg.io.dataReadingTools as REeg
from os.path import basename, abspath
from pyeeg.eeg_processing_tools.acc_time_tools import *
from pyeeg.eeg_processing_tools.abr_tools import *
from pyeeg.io.external_tools.aep_gui.aep_matlab_tools import *
import pandas as pd
from pyeeg.definitions.eeg_definitions import Domain, MeasurementInformation
from pyeeg.io.storage.data_storage_tools import store_data
import re
import gc
import numbers
__author__ = 'jundurraga-ucl'


class BColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def get_responses_from_path(path='', **kwargs):
    response_type = kwargs.pop('response_type', 'ipm_fr')
    ref_ch = kwargs.pop('ref_channels', [])
    oeg_ch = kwargs.pop('oeg_channels', [])
    bad_ch = kwargs.pop('bad_channels', [])
    ignore_log_folders = kwargs.pop('ignore_log_folders', False)  # ignore folders where there is a log_file
    signal_info = kwargs.pop('signal_info', None)
    measurement_info = kwargs.pop('measurement_info', None)
    save_to_data_base = kwargs.pop('save_to_data_base', False)
    data_base_path = kwargs.pop('data_base_path', '')
    experiment = kwargs.pop('experiment', 'assr')
    plot_title_parameters = kwargs.pop('plot_title_parameters', [])
    save_single_recordings = kwargs.pop('save_single_recordings', True)
    sf_frequencies = kwargs.pop('sf_frequencies', None)
    save_component_maps = kwargs.pop('save_component_maps', True)
    roi_windows = kwargs.pop('roi_windows', None)

    if (ignore_log_folders and isfile(abspath(path) + sep + 'log_file.log')) or \
            (isfile(abspath(path) + sep + 'ignore.tex')):
        print('ignoring path: ', path)
        return

    data_measurements = REeg.get_measurement_and_triggers(path,
                                                          **kwargs)
    for i, measurement in enumerate(data_measurements):
        # try:
        if signal_info is None:
            stimulus_parameters = get_stimulus_parameters(measurement['parameters'])
        else:
            stimulus_parameters = [signal_information(**signal_info[i])]

        for _stimulus_c in stimulus_parameters:
            if _stimulus_c['condition'] is None:
                _stimulus_c['condition'] = os.path.basename(measurement['data_reader'].file_name).split('.')[0] \
                                        + '_' + str(i)

        if measurement_info is None:
            _measurement_info = MeasurementInformation()
        elif len(measurement_info) == len(data_measurements):
            _measurement_info = measurement_info[i]
        else:
            print("Provided measurement info does not match number of measurements %i, %i" %\
                  (len(measurement_info) if measurement_info else measurement_info, len(data_measurements)))

        _channels = measurement['data_reader'].all_channels
        oeg_channels = measurement['data_reader'].get_channel_by_label(oeg_ch)
        bad_channels = [ch for ch in _channels if ch.label in bad_ch]
        if ref_ch is None:
            ref_channels = _channels
        else:
            ref_channels = [ch for ch in _channels if ch.label in ref_ch]
        if basename(measurement['parameters_file_name']).split('.')[0]:
            data_name = basename(measurement['parameters_file_name']).split('.')[0]
        else:
            data_name = basename(measurement['data_reader'].file_name).split('.')[0]

        print((''.join([BColors.OKGREEN + 'Current stimulus: \n' + pd.DataFrame([_stim]).to_string() +
                        BColors.ENDC for _stim in stimulus_parameters])))
        title_params = []
        for _stimulus_c in stimulus_parameters:
            _stimulus = _stimulus_c
            title_params.append([item + ':' + '{:1.1e}'.format(_stimulus[item]).strip() +
                                 '/' for i, item in enumerate(plot_title_parameters)
                                 if item in list(_stimulus.keys()) and
                                 isinstance(_stimulus[item], numbers.Number)])
        if _measurement_info.subject is None:
            if 'parameters' in measurement and measurement['parameters'] and 'Measurement' in measurement['parameters']:
                _measurement_info.subject = measurement['parameters']['Measurement']['MeasurementModule']['Subject']
            else:
                measurement_info.subject = os.path.basename(measurement['data_reader'].file_name).split('.')[0]
        if _measurement_info.condition is None:
            _measurement_info.condition = os.path.basename(measurement['data_reader'].file_name).split('.')[0]

        _title = _measurement_info.subject + '/'
        title_params, _idx_title = np.unique(title_params, return_index=True)
        for _idx in _idx_title:
            _title += '\n'.join([''.join(title_params[_idx])])
        _title = re.sub('[a-z]', '', _title)

        subset_identifier = _measurement_info.subject
        for _stimulus_c in stimulus_parameters:
            _stimulus = _stimulus_c

            subset_identifier += ''.join([item + '_' + '{:1.1e}'.format(_stimulus[item]).strip() + '_'
                                          for i, item in enumerate(plot_title_parameters)
                                          if item in list(_stimulus.keys()) and (isinstance(_stimulus[item], float) or
                                                                           isinstance(_stimulus[item], int))])
            subset_identifier += ''.join([item + '_' + _stimulus[item] + '_'
                                          for i, item in enumerate(plot_title_parameters)
                                          if item in list(_stimulus.keys()) and isinstance(_stimulus[item], str)])
        subset_identifier = re.sub('[a-z]', '', subset_identifier)

        if response_type in ['ipm_fr', 'assr', 'fr']:
            if sf_frequencies is None:
                _sf_frequencies = get_frequencies_of_interest(stimulus_parameters=stimulus_parameters)
            else:
                _sf_frequencies = sf_frequencies
            # todo: interpolation_data_points = get_interpolation_data_points(**kwargs)
            # get information from parameters
            get_time_ep(data_reader=measurement['data_reader'],
                        measurement_info=_measurement_info,
                        stimulus_parameters=stimulus_parameters,
                        triggers=measurement['triggers'],
                        data_name=data_name,
                        path=path,
                        ref_channels=ref_channels,
                        oeg_channels=oeg_channels,
                        bad_channels=bad_channels,
                        sf_join_frequencies=_sf_frequencies,
                        test_frequencies=_sf_frequencies,
                        average_domain=Domain.frequency,
                        title=_title,
                        subset_identifier=subset_identifier,
                        **kwargs)
            if save_to_data_base:
                # save data
                concatenated_stimuli = cat_dictionary_list(dict_list=[_stim for _stim in stimulus_parameters])
                recording = dict(fs=measurement['data_reader'].fs,
                                 system=measurement['data_reader'].system,
                                 low_pass_filter=measurement['data_reader'].low_pass_freq,
                                 high_pass_filter=measurement['data_reader'].high_pass_freq,
                                 notch_filter=str(measurement['data_reader'].notch_frequencies),
                                 sf_number_comp=str(measurement['data_reader'].spatial_filter.component_indexes))

                store_data(data_base_path=data_base_path,
                           subject=_measurement_info.subject,
                           experiment=experiment,
                           measurement_info=_measurement_info,
                           stimuli=concatenated_stimuli,
                           recording=recording,
                           frequency_peaks=measurement['data_reader'].epochs_processed_ave.peak_frequency,
                           time_peaks=measurement['data_reader'].epochs_processed_ave.peak_times,
                           peak_amplitudes=measurement['data_reader'].epochs_processed_ave.peak_amplitudes,
                           epochs_ave=measurement['data_reader'].epochs_processed_ave,
                           save_single_recordings=save_single_recordings,
                           save_component_maps=save_component_maps,
                           component_maps=measurement['data_reader'].component_maps_ave)

        if response_type in ['acc', 'abr']:
            # todo: interpolation_data_points = get_interpolation_data_points(**kwargs)
            # get information from parameters
            get_time_ep(data_reader=measurement['data_reader'],
                        measurement_info=_measurement_info,
                        stimulus_parameters=stimulus_parameters,
                        triggers=measurement['triggers'],
                        data_name=data_name,
                        path=path,
                        ref_channels=ref_channels,
                        oeg_channels=oeg_channels,
                        bad_channels=bad_channels,
                        title=_title,
                        subset_identifier=subset_identifier,
                        roi_windows=roi_windows,
                        **kwargs)

            if save_to_data_base:
                # save data
                concatenated_stimuli = cat_dictionary_list(dict_list=[_stim for _stim in stimulus_parameters])
                recording = dict(fs=measurement['data_reader'].fs,
                                 system=measurement['data_reader'].system,
                                 low_pass_filter=measurement['data_reader'].low_pass_freq,
                                 high_pass_filter=measurement['data_reader'].high_pass_freq,
                                 notch_filter=str(measurement['data_reader'].notch_frequencies),
                                 sf_number_comp=str(measurement['data_reader'].spatial_filter.component_indexes))

                store_data(data_base_path=data_base_path,
                           subject=_measurement_info.subject,
                           experiment=experiment,
                           measurement_info=_measurement_info,
                           stimuli=concatenated_stimuli,
                           recording=recording,
                           roi_windows=roi_windows,
                           hotelling_t2_tests=measurement['data_reader'].hotelling_t2_test(**kwargs),
                           time_peaks=measurement['data_reader'].epochs_processed_ave.peak_times,
                           peak_amplitudes=measurement['data_reader'].epochs_processed_ave.peak_amplitudes,
                           epochs_ave=measurement['data_reader'].epochs_processed_ave,
                           save_single_recordings=save_single_recordings,
                           component_maps=measurement['data_reader'].component_maps_ave)
        # data_measurements.pop(i)
        del measurement['data_reader']
        gc.collect()
        # except ValueError:
        #     print('Error processing file:' + basename(measurement['parameters_file_name']))
        #     continue
