from mne.io import read_raw_edf
from mne import find_events
from mne_bids import raw_to_bids
from mne_bids.utils import print_dir_tree

# First, tell `MNE-BIDS` that it is working with EEG data:
kind = 'eeg'

# Brief description of the event markers present in the data. This will become
# the `trial_type` column in our BIDS `events.tsv`. We know about the event
# meaning from the documentation on PhysioBank
edf_path = '/home/jundurraga/Documents/source_code/pyeeg/test_data/sub-01_ses-test_task-ipmfr90_ieeg.bdf'
output_path = '/home/jundurraga/Documents/source_code/pyeeg/test_data/bids'
raw = read_raw_edf(edf_path, preload=True)
trial_type = {'itd_switch': 253.0}
e = find_events(raw, mask=255)
# Now convert our data to be in a new BIDS dataset.
raw_to_bids(subject_id='001',
            task='assr',
            session_id='initial',
            run='1',
            raw_file=raw,
            output_path=output_path,
            kind=kind,
            event_id=trial_type,
            events_data=e,
            overwrite=True)

print_dir_tree(output_path)